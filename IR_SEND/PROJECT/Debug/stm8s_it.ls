   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
  33                     ; 47 INTERRUPT_HANDLER(NonHandledInterrupt, 25)
  33                     ; 48 {
  34                     	switch	.text
  35  0000               f_NonHandledInterrupt:
  40                     ; 52 }
  43  0000 80            	iret
  65                     ; 60 INTERRUPT_HANDLER_TRAP(TRAP_IRQHandler)
  65                     ; 61 {
  66                     	switch	.text
  67  0001               f_TRAP_IRQHandler:
  72                     ; 65 }
  75  0001 80            	iret
  97                     ; 71 INTERRUPT_HANDLER(TLI_IRQHandler, 0)
  97                     ; 72 
  97                     ; 73 {
  98                     	switch	.text
  99  0002               f_TLI_IRQHandler:
 104                     ; 77 }
 107  0002 80            	iret
 129                     ; 84 INTERRUPT_HANDLER(AWU_IRQHandler, 1)
 129                     ; 85 {
 130                     	switch	.text
 131  0003               f_AWU_IRQHandler:
 136                     ; 89 }
 139  0003 80            	iret
 161                     ; 96 INTERRUPT_HANDLER(CLK_IRQHandler, 2)
 161                     ; 97 {
 162                     	switch	.text
 163  0004               f_CLK_IRQHandler:
 168                     ; 101 }
 171  0004 80            	iret
 194                     ; 108 INTERRUPT_HANDLER(EXTI_PORTA_IRQHandler, 3)
 194                     ; 109 {
 195                     	switch	.text
 196  0005               f_EXTI_PORTA_IRQHandler:
 201                     ; 113 }
 204  0005 80            	iret
 227                     ; 120 INTERRUPT_HANDLER(EXTI_PORTB_IRQHandler, 4)
 227                     ; 121 {
 228                     	switch	.text
 229  0006               f_EXTI_PORTB_IRQHandler:
 234                     ; 125 }
 237  0006 80            	iret
 260                     ; 132 INTERRUPT_HANDLER(EXTI_PORTC_IRQHandler, 5)
 260                     ; 133 {
 261                     	switch	.text
 262  0007               f_EXTI_PORTC_IRQHandler:
 267                     ; 137 }
 270  0007 80            	iret
 293                     ; 144 INTERRUPT_HANDLER(EXTI_PORTD_IRQHandler, 6)
 293                     ; 145 {
 294                     	switch	.text
 295  0008               f_EXTI_PORTD_IRQHandler:
 300                     ; 150 }
 303  0008 80            	iret
 326                     ; 157 INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
 326                     ; 158 {
 327                     	switch	.text
 328  0009               f_EXTI_PORTE_IRQHandler:
 333                     ; 170 }
 336  0009 80            	iret
 358                     ; 217 INTERRUPT_HANDLER(SPI_IRQHandler, 10)
 358                     ; 218 {
 359                     	switch	.text
 360  000a               f_SPI_IRQHandler:
 365                     ; 222 }
 368  000a 80            	iret
 394                     ; 229 INTERRUPT_HANDLER(TIM1_UPD_OVF_TRG_BRK_IRQHandler, 11)
 394                     ; 230 {
 395                     	switch	.text
 396  000b               f_TIM1_UPD_OVF_TRG_BRK_IRQHandler:
 399  000b 3b0002        	push	c_x+2
 400  000e be00          	ldw	x,c_x
 401  0010 89            	pushw	x
 402  0011 3b0002        	push	c_y+2
 403  0014 be00          	ldw	x,c_y
 404  0016 89            	pushw	x
 407                     ; 234 	TIM1_ClearFlag(TIM1_FLAG_UPDATE);
 409  0017 ae0001        	ldw	x,#1
 410  001a cd0000        	call	_TIM1_ClearFlag
 412                     ; 235   	Key_InterruptHandler();	
 414  001d cd0000        	call	_Key_InterruptHandler
 416                     ; 236 	SYSTEM_TimeToClose_IQRHandle();
 418  0020 cd0000        	call	_SYSTEM_TimeToClose_IQRHandle
 420                     ; 238 }
 423  0023 85            	popw	x
 424  0024 bf00          	ldw	c_y,x
 425  0026 320002        	pop	c_y+2
 426  0029 85            	popw	x
 427  002a bf00          	ldw	c_x,x
 428  002c 320002        	pop	c_x+2
 429  002f 80            	iret
 452                     ; 245 INTERRUPT_HANDLER(TIM1_CAP_COM_IRQHandler, 12)
 452                     ; 246 {
 453                     	switch	.text
 454  0030               f_TIM1_CAP_COM_IRQHandler:
 459                     ; 250 }
 462  0030 80            	iret
 485                     ; 284  INTERRUPT_HANDLER(TIM2_UPD_OVF_BRK_IRQHandler, 13)
 485                     ; 285 {
 486                     	switch	.text
 487  0031               f_TIM2_UPD_OVF_BRK_IRQHandler:
 492                     ; 289 }
 495  0031 80            	iret
 518                     ; 296  INTERRUPT_HANDLER(TIM2_CAP_COM_IRQHandler, 14)
 518                     ; 297 {
 519                     	switch	.text
 520  0032               f_TIM2_CAP_COM_IRQHandler:
 525                     ; 301 }
 528  0032 80            	iret
 551                     ; 338  INTERRUPT_HANDLER(UART1_TX_IRQHandler, 17)
 551                     ; 339  {
 552                     	switch	.text
 553  0033               f_UART1_TX_IRQHandler:
 558                     ; 343  }
 561  0033 80            	iret
 584                     ; 350  INTERRUPT_HANDLER(UART1_RX_IRQHandler, 18)
 584                     ; 351  {
 585                     	switch	.text
 586  0034               f_UART1_RX_IRQHandler:
 591                     ; 355  }
 594  0034 80            	iret
 616                     ; 363 INTERRUPT_HANDLER(I2C_IRQHandler, 19)
 616                     ; 364 {
 617                     	switch	.text
 618  0035               f_I2C_IRQHandler:
 623                     ; 368 }
 626  0035 80            	iret
 648                     ; 442  INTERRUPT_HANDLER(ADC1_IRQHandler, 22)
 648                     ; 443  {
 649                     	switch	.text
 650  0036               f_ADC1_IRQHandler:
 655                     ; 447  }
 658  0036 80            	iret
 683                     ; 468  INTERRUPT_HANDLER(TIM4_UPD_OVF_IRQHandler, 23)
 683                     ; 469  {
 684                     	switch	.text
 685  0037               f_TIM4_UPD_OVF_IRQHandler:
 688  0037 3b0002        	push	c_x+2
 689  003a be00          	ldw	x,c_x
 690  003c 89            	pushw	x
 691  003d 3b0002        	push	c_y+2
 692  0040 be00          	ldw	x,c_y
 693  0042 89            	pushw	x
 696                     ; 473   	TIM4_ClearFlag(TIM4_FLAG_UPDATE);
 698  0043 a601          	ld	a,#1
 699  0045 cd0000        	call	_TIM4_ClearFlag
 701                     ; 474 	TimeOutDet_DecHandle();
 703  0048 cd0000        	call	_TimeOutDet_DecHandle
 705                     ; 477  }
 708  004b 85            	popw	x
 709  004c bf00          	ldw	c_y,x
 710  004e 320002        	pop	c_y+2
 711  0051 85            	popw	x
 712  0052 bf00          	ldw	c_x,x
 713  0054 320002        	pop	c_x+2
 714  0057 80            	iret
 737                     ; 485 INTERRUPT_HANDLER(EEPROM_EEC_IRQHandler, 24)
 737                     ; 486 {
 738                     	switch	.text
 739  0058               f_EEPROM_EEC_IRQHandler:
 744                     ; 491 }
 747  0058 80            	iret
 759                     	xdef	f_EEPROM_EEC_IRQHandler
 760                     	xdef	f_TIM4_UPD_OVF_IRQHandler
 761                     	xdef	f_ADC1_IRQHandler
 762                     	xdef	f_I2C_IRQHandler
 763                     	xdef	f_UART1_RX_IRQHandler
 764                     	xdef	f_UART1_TX_IRQHandler
 765                     	xdef	f_TIM2_CAP_COM_IRQHandler
 766                     	xdef	f_TIM2_UPD_OVF_BRK_IRQHandler
 767                     	xdef	f_TIM1_UPD_OVF_TRG_BRK_IRQHandler
 768                     	xdef	f_TIM1_CAP_COM_IRQHandler
 769                     	xdef	f_SPI_IRQHandler
 770                     	xdef	f_EXTI_PORTE_IRQHandler
 771                     	xdef	f_EXTI_PORTD_IRQHandler
 772                     	xdef	f_EXTI_PORTC_IRQHandler
 773                     	xdef	f_EXTI_PORTB_IRQHandler
 774                     	xdef	f_EXTI_PORTA_IRQHandler
 775                     	xdef	f_CLK_IRQHandler
 776                     	xdef	f_AWU_IRQHandler
 777                     	xdef	f_TLI_IRQHandler
 778                     	xdef	f_TRAP_IRQHandler
 779                     	xdef	f_NonHandledInterrupt
 780                     	xref	_Key_InterruptHandler
 781                     	xref	_SYSTEM_TimeToClose_IQRHandle
 782                     	xref	_TimeOutDet_DecHandle
 783                     	xref	_TIM4_ClearFlag
 784                     	xref	_TIM1_ClearFlag
 785                     	xref.b	c_x
 786                     	xref.b	c_y
 805                     	end
