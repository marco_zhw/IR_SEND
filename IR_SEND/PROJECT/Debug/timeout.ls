   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
   4                     	bsct
   5  0000               _InsBaseNum:
   6  0000 00000000      	dc.l	0
  36                     ; 6 void TimeOutDet_Init(void)
  36                     ; 7 {
  38                     	switch	.text
  39  0000               _TimeOutDet_Init:
  43                     ; 8 	InsBaseNum=0;
  45  0000 ae0000        	ldw	x,#0
  46  0003 bf02          	ldw	_InsBaseNum+2,x
  47  0005 ae0000        	ldw	x,#0
  48  0008 bf00          	ldw	_InsBaseNum,x
  49                     ; 9 }
  52  000a 81            	ret
  77                     ; 12 void TimeOutDet_DecHandle(void)
  77                     ; 13 {
  78                     	switch	.text
  79  000b               _TimeOutDet_DecHandle:
  83                     ; 14 	InsBaseNum++;
  85  000b ae0000        	ldw	x,#_InsBaseNum
  86  000e a601          	ld	a,#1
  87  0010 cd0000        	call	c_lgadc
  89                     ; 15 }
  92  0013 81            	ret
 116                     ; 17 clock_time_t TimeOut_BaseValue(void)
 116                     ; 18 {
 117                     	switch	.text
 118  0014               _TimeOut_BaseValue:
 122                     ; 19 	return InsBaseNum;
 124  0014 ae0000        	ldw	x,#_InsBaseNum
 125  0017 cd0000        	call	c_ltor
 129  001a 81            	ret
 197                     ; 23 void TimeOut_Record(TIMEOUT_PARA * TimeOutPara,unsigned int timeoutvalue)
 197                     ; 24 {
 198                     	switch	.text
 199  001b               _TimeOut_Record:
 201  001b 89            	pushw	x
 202       00000000      OFST:	set	0
 205                     ; 25 	TimeOutPara->RecordValue=TimeOut_BaseValue();
 207  001c adf6          	call	_TimeOut_BaseValue
 209  001e 1e01          	ldw	x,(OFST+1,sp)
 210  0020 cd0000        	call	c_rtol
 212                     ; 26 	TimeOutPara->timeoutcnt=timeoutvalue;
 214  0023 1e05          	ldw	x,(OFST+5,sp)
 215  0025 cd0000        	call	c_uitolx
 217  0028 1e01          	ldw	x,(OFST+1,sp)
 218  002a 1c0004        	addw	x,#4
 219  002d cd0000        	call	c_rtol
 221                     ; 27 }
 224  0030 85            	popw	x
 225  0031 81            	ret
 263                     ; 30 void TimeOut_restart(TIMEOUT_PARA *TimeOutPara)
 263                     ; 31 {	
 264                     	switch	.text
 265  0032               _TimeOut_restart:
 267  0032 89            	pushw	x
 268       00000000      OFST:	set	0
 271                     ; 32 	TimeOutPara->RecordValue = TimeOut_BaseValue();
 273  0033 addf          	call	_TimeOut_BaseValue
 275  0035 1e01          	ldw	x,(OFST+1,sp)
 276  0037 cd0000        	call	c_rtol
 278                     ; 33 }
 281  003a 85            	popw	x
 282  003b 81            	ret
 320                     ; 35 clock_time_t TimeOutDet_Check(TIMEOUT_PARA * TimeOutPara)
 320                     ; 36 {
 321                     	switch	.text
 322  003c               _TimeOutDet_Check:
 324  003c 89            	pushw	x
 325       00000000      OFST:	set	0
 328                     ; 37 	return (((clock_time_t)(InsBaseNum-TimeOutPara->RecordValue)>=TimeOutPara->timeoutcnt)?1:0);
 330  003d ae0000        	ldw	x,#_InsBaseNum
 331  0040 cd0000        	call	c_ltor
 333  0043 1e01          	ldw	x,(OFST+1,sp)
 334  0045 cd0000        	call	c_lsub
 336  0048 1e01          	ldw	x,(OFST+1,sp)
 337  004a 1c0004        	addw	x,#4
 338  004d cd0000        	call	c_lcmp
 340  0050 2505          	jrult	L02
 341  0052 ae0001        	ldw	x,#1
 342  0055 2001          	jra	L22
 343  0057               L02:
 344  0057 5f            	clrw	x
 345  0058               L22:
 346  0058 cd0000        	call	c_itolx
 350  005b 5b02          	addw	sp,#2
 351  005d 81            	ret
 375                     	xdef	_InsBaseNum
 376                     	xdef	_TimeOutDet_Check
 377                     	xdef	_TimeOut_restart
 378                     	xdef	_TimeOut_Record
 379                     	xdef	_TimeOut_BaseValue
 380                     	xdef	_TimeOutDet_DecHandle
 381                     	xdef	_TimeOutDet_Init
 400                     	xref	c_itolx
 401                     	xref	c_lcmp
 402                     	xref	c_lsub
 403                     	xref	c_uitolx
 404                     	xref	c_rtol
 405                     	xref	c_ltor
 406                     	xref	c_lgadc
 407                     	end
