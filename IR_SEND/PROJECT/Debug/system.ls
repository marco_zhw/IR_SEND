   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
  38                     ; 10 void SYSTEM_TIM1_Init(void)
  38                     ; 11 {
  40                     	switch	.text
  41  0000               _SYSTEM_TIM1_Init:
  45                     ; 12 	TIM1_DeInit();
  47  0000 cd0000        	call	_TIM1_DeInit
  49                     ; 13 	TIM1_TimeBaseInit(TIM1_Prescaler_8,TIM2_PSCRELOADMODE_UPDATE,Tim1_Period_1MS,0);   //1ms
  51  0003 4b00          	push	#0
  52  0005 ae03e8        	ldw	x,#1000
  53  0008 89            	pushw	x
  54  0009 4b00          	push	#0
  55  000b ae0007        	ldw	x,#7
  56  000e cd0000        	call	_TIM1_TimeBaseInit
  58  0011 5b04          	addw	sp,#4
  59                     ; 14 	TIM1_ITConfig(TIM1_IT_UPDATE,ENABLE);
  61  0013 ae0001        	ldw	x,#1
  62  0016 a601          	ld	a,#1
  63  0018 95            	ld	xh,a
  64  0019 cd0000        	call	_TIM1_ITConfig
  66                     ; 15 	TIM1_UpdateDisableConfig(DISABLE);
  68  001c 4f            	clr	a
  69  001d cd0000        	call	_TIM1_UpdateDisableConfig
  71                     ; 16 	TIM1_UpdateRequestConfig( TIM1_UPDATESOURCE_GLOBAL);
  73  0020 4f            	clr	a
  74  0021 cd0000        	call	_TIM1_UpdateRequestConfig
  76                     ; 17 	TIM1_Cmd(ENABLE);
  78  0024 a601          	ld	a,#1
  79  0026 cd0000        	call	_TIM1_Cmd
  81                     ; 18 }
  84  0029 81            	ret
 115                     ; 20 void SYSTEM_TIM4_Init(void)
 115                     ; 21 {
 116                     	switch	.text
 117  002a               _SYSTEM_TIM4_Init:
 121                     ; 22 	TIM4_DeInit();
 123  002a cd0000        	call	_TIM4_DeInit
 125                     ; 23 	TIM4_TimeBaseInit( TIM4_PRESCALER_8 ,TIM4_Delay90us-2);///90us
 127  002d ae0058        	ldw	x,#88
 128  0030 a603          	ld	a,#3
 129  0032 95            	ld	xh,a
 130  0033 cd0000        	call	_TIM4_TimeBaseInit
 132                     ; 24 	TIM4_PrescalerConfig(TIM4_PRESCALER_8,TIM4_PSCRELOADMODE_IMMEDIATE);
 134  0036 ae0001        	ldw	x,#1
 135  0039 a603          	ld	a,#3
 136  003b 95            	ld	xh,a
 137  003c cd0000        	call	_TIM4_PrescalerConfig
 139                     ; 25 	TIM4_ARRPreloadConfig(ENABLE);
 141  003f a601          	ld	a,#1
 142  0041 cd0000        	call	_TIM4_ARRPreloadConfig
 144                     ; 26 	TIM4_ITConfig( TIM4_IT_UPDATE , ENABLE);
 146  0044 ae0001        	ldw	x,#1
 147  0047 a601          	ld	a,#1
 148  0049 95            	ld	xh,a
 149  004a cd0000        	call	_TIM4_ITConfig
 151                     ; 27 	TIM4_UpdateDisableConfig(DISABLE);
 153  004d 4f            	clr	a
 154  004e cd0000        	call	_TIM4_UpdateDisableConfig
 156                     ; 28 	TIM4_UpdateRequestConfig( TIM4_UPDATESOURCE_GLOBAL);
 158  0051 4f            	clr	a
 159  0052 cd0000        	call	_TIM4_UpdateRequestConfig
 161                     ; 29 	TIM4_Cmd(ENABLE );
 163  0055 a601          	ld	a,#1
 164  0057 cd0000        	call	_TIM4_Cmd
 166                     ; 30 }
 169  005a 81            	ret
 198                     ; 31 void SYSTEM_TIM2_PWM_Init(void)
 198                     ; 32 {
 199                     	switch	.text
 200  005b               _SYSTEM_TIM2_PWM_Init:
 204                     ; 34 	TIM2_TimeBaseInit(TIM2_PRESCALER_16,12);
 206  005b ae000c        	ldw	x,#12
 207  005e 89            	pushw	x
 208  005f a604          	ld	a,#4
 209  0061 cd0000        	call	_TIM2_TimeBaseInit
 211  0064 85            	popw	x
 212                     ; 36 	TIM2_OC3Init(TIM2_OCMODE_PWM1, TIM2_OUTPUTSTATE_ENABLE,5, TIM2_OCPOLARITY_HIGH);
 214  0065 4b00          	push	#0
 215  0067 ae0005        	ldw	x,#5
 216  006a 89            	pushw	x
 217  006b ae0011        	ldw	x,#17
 218  006e a660          	ld	a,#96
 219  0070 95            	ld	xh,a
 220  0071 cd0000        	call	_TIM2_OC3Init
 222  0074 5b03          	addw	sp,#3
 223                     ; 37 	TIM2_OC3PreloadConfig(ENABLE);
 225  0076 a601          	ld	a,#1
 226  0078 cd0000        	call	_TIM2_OC3PreloadConfig
 228                     ; 38 	TIM2_ARRPreloadConfig(ENABLE);
 230  007b a601          	ld	a,#1
 231  007d cd0000        	call	_TIM2_ARRPreloadConfig
 233                     ; 40 	TIM2_PWM_DISABLE();
 235  0080 4f            	clr	a
 236  0081 cd0000        	call	_TIM2_Cmd
 238                     ; 42 }
 241  0084 81            	ret
 273                     ; 43 void SYSTEM_Function_Init(void)
 273                     ; 44 {
 274                     	switch	.text
 275  0085               _SYSTEM_Function_Init:
 279                     ; 45 	CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV2);//MCU MAIN CLOCK=8MHZ 
 281  0085 a608          	ld	a,#8
 282  0087 cd0000        	call	_CLK_SYSCLKConfig
 284                     ; 47 	SYSTEM_Led_Init(); 
 286  008a 4bf0          	push	#240
 287  008c 4b10          	push	#16
 288  008e ae500a        	ldw	x,#20490
 289  0091 cd0000        	call	_GPIO_Init
 291  0094 85            	popw	x
 294  0095 4bf0          	push	#240
 295  0097 4b20          	push	#32
 296  0099 ae500a        	ldw	x,#20490
 297  009c cd0000        	call	_GPIO_Init
 299  009f 85            	popw	x
 302  00a0 4bf0          	push	#240
 303  00a2 4b40          	push	#64
 304  00a4 ae500a        	ldw	x,#20490
 305  00a7 cd0000        	call	_GPIO_Init
 307  00aa 85            	popw	x
 310  00ab 4bf0          	push	#240
 311  00ad 4b80          	push	#128
 312  00af ae500a        	ldw	x,#20490
 313  00b2 cd0000        	call	_GPIO_Init
 315  00b5 85            	popw	x
 318  00b6 4b10          	push	#16
 319  00b8 ae500a        	ldw	x,#20490
 320  00bb cd0000        	call	_GPIO_WriteLow
 322  00be 84            	pop	a
 325  00bf 4b20          	push	#32
 326  00c1 ae500a        	ldw	x,#20490
 327  00c4 cd0000        	call	_GPIO_WriteLow
 329  00c7 84            	pop	a
 332  00c8 4b40          	push	#64
 333  00ca ae500a        	ldw	x,#20490
 334  00cd cd0000        	call	_GPIO_WriteLow
 336  00d0 84            	pop	a
 339  00d1 4b80          	push	#128
 340  00d3 ae500a        	ldw	x,#20490
 341  00d6 cd0000        	call	_GPIO_WriteLow
 343  00d9 84            	pop	a
 344                     ; 49 	SYSTEM_IR_Init();
 347  00da 4be0          	push	#224
 348  00dc 4b08          	push	#8
 349  00de ae500f        	ldw	x,#20495
 350  00e1 cd0000        	call	_GPIO_Init
 352  00e4 85            	popw	x
 355  00e5 4b08          	push	#8
 356  00e7 ae500f        	ldw	x,#20495
 357  00ea cd0000        	call	_GPIO_WriteLow
 359  00ed 84            	pop	a
 360                     ; 51 	KEY_Init();
 363  00ee 4b40          	push	#64
 364  00f0 4b08          	push	#8
 365  00f2 ae500a        	ldw	x,#20490
 366  00f5 cd0000        	call	_GPIO_Init
 368  00f8 85            	popw	x
 369                     ; 53 	SYSTEM_TIM1_Init();
 371  00f9 cd0000        	call	_SYSTEM_TIM1_Init
 373                     ; 55 	SYSTEM_TIM4_Init();
 375  00fc cd002a        	call	_SYSTEM_TIM4_Init
 377                     ; 57 	SP_INIT(&(System_Para.sp_thd));
 379  00ff 3f09          	clr	_System_Para+9
 380                     ; 58 	TimeOutDet_Init();
 383  0101 cd0000        	call	_TimeOutDet_Init
 385                     ; 60 	SYSTEM_TIM2_PWM_Init();
 387  0104 cd005b        	call	_SYSTEM_TIM2_PWM_Init
 389                     ; 61 }
 392  0107 81            	ret
 418                     ; 63 void SYSTEM_LED_Handle(void)
 418                     ; 64 {
 419                     	switch	.text
 420  0108               _SYSTEM_LED_Handle:
 424                     ; 65 	switch(System_Para.System_Led_Status)
 426  0108 b600          	ld	a,_System_Para
 428                     ; 141 			break;
 429  010a 4d            	tnz	a
 430  010b 271b          	jreq	L15
 431  010d 4a            	dec	a
 432  010e 2753          	jreq	L35
 433  0110 4a            	dec	a
 434  0111 2603cc019e    	jreq	L55
 435  0116 4a            	dec	a
 436  0117 2603          	jrne	L61
 437  0119 cc01d7        	jp	L75
 438  011c               L61:
 439  011c 4a            	dec	a
 440  011d 2603          	jrne	L02
 441  011f cc0210        	jp	L16
 442  0122               L02:
 443  0122               L36:
 444                     ; 140 			System_Para.System_Led_Status=SYSTEM_LED_ALL_OFF;
 446  0122 3f00          	clr	_System_Para
 447                     ; 141 			break;
 449  0124 ac470247      	jpf	L77
 450  0128               L15:
 451                     ; 69 			System_Para._1HourClose_Flag=False;
 453  0128 72110014      	bres	_System_Para+20,#0
 454                     ; 70 			System_Para._2HourClose_Flag=False;
 456  012c 72130014      	bres	_System_Para+20,#1
 457                     ; 71 			System_Para._3HourClose_Flag=False;
 459  0130 72150014      	bres	_System_Para+20,#2
 460                     ; 72 			System_Para._4HourClose_Flag=False;
 462  0134 72170014      	bres	_System_Para+20,#3
 463                     ; 73 			System_Para.CloseCnt_1S=0;
 465  0138 5f            	clrw	x
 466  0139 bf10          	ldw	_System_Para+16,x
 467                     ; 75 			GPIO_WriteLow(_SYSTEM_1H_LED_PORT,_SYSTEM_1H_LED_PIN);
 469  013b 4b10          	push	#16
 470  013d ae500a        	ldw	x,#20490
 471  0140 cd0000        	call	_GPIO_WriteLow
 473  0143 84            	pop	a
 474                     ; 76 			GPIO_WriteLow(_SYSTEM_2H_LED_PORT,_SYSTEM_2H_LED_PIN);
 476  0144 4b20          	push	#32
 477  0146 ae500a        	ldw	x,#20490
 478  0149 cd0000        	call	_GPIO_WriteLow
 480  014c 84            	pop	a
 481                     ; 77 			GPIO_WriteLow(_SYSTEM_3H_LED_PORT,_SYSTEM_3H_LED_PIN);
 483  014d 4b40          	push	#64
 484  014f ae500a        	ldw	x,#20490
 485  0152 cd0000        	call	_GPIO_WriteLow
 487  0155 84            	pop	a
 488                     ; 78 			GPIO_WriteLow(_SYSTEM_4H_LED_PORT,_SYSTEM_4H_LED_PIN);
 490  0156 4b80          	push	#128
 491  0158 ae500a        	ldw	x,#20490
 492  015b cd0000        	call	_GPIO_WriteLow
 494  015e 84            	pop	a
 495                     ; 79 			break;
 497  015f ac470247      	jpf	L77
 498  0163               L35:
 499                     ; 83 			System_Para._1HourClose_Flag=True;
 501  0163 72100014      	bset	_System_Para+20,#0
 502                     ; 84 			System_Para._2HourClose_Flag=False;
 504  0167 72130014      	bres	_System_Para+20,#1
 505                     ; 85 			System_Para._3HourClose_Flag=False;
 507  016b 72150014      	bres	_System_Para+20,#2
 508                     ; 86 			System_Para._4HourClose_Flag=False;
 510  016f 72170014      	bres	_System_Para+20,#3
 511                     ; 87 			System_Para.CloseCnt_1S=0;
 513  0173 5f            	clrw	x
 514  0174 bf10          	ldw	_System_Para+16,x
 515                     ; 89 			GPIO_WriteHigh(_SYSTEM_1H_LED_PORT,_SYSTEM_1H_LED_PIN);
 517  0176 4b10          	push	#16
 518  0178 ae500a        	ldw	x,#20490
 519  017b cd0000        	call	_GPIO_WriteHigh
 521  017e 84            	pop	a
 522                     ; 90 			GPIO_WriteLow(_SYSTEM_2H_LED_PORT,_SYSTEM_2H_LED_PIN);
 524  017f 4b20          	push	#32
 525  0181 ae500a        	ldw	x,#20490
 526  0184 cd0000        	call	_GPIO_WriteLow
 528  0187 84            	pop	a
 529                     ; 91 			GPIO_WriteLow(_SYSTEM_3H_LED_PORT,_SYSTEM_3H_LED_PIN);
 531  0188 4b40          	push	#64
 532  018a ae500a        	ldw	x,#20490
 533  018d cd0000        	call	_GPIO_WriteLow
 535  0190 84            	pop	a
 536                     ; 92 			GPIO_WriteLow(_SYSTEM_4H_LED_PORT,_SYSTEM_4H_LED_PIN);
 538  0191 4b80          	push	#128
 539  0193 ae500a        	ldw	x,#20490
 540  0196 cd0000        	call	_GPIO_WriteLow
 542  0199 84            	pop	a
 543                     ; 94 			break;
 545  019a ac470247      	jpf	L77
 546  019e               L55:
 547                     ; 98 			System_Para._1HourClose_Flag=False;
 549  019e 72110014      	bres	_System_Para+20,#0
 550                     ; 99 			System_Para._2HourClose_Flag=True;
 552  01a2 72120014      	bset	_System_Para+20,#1
 553                     ; 100 			System_Para._3HourClose_Flag=False;
 555  01a6 72150014      	bres	_System_Para+20,#2
 556                     ; 101 			System_Para._4HourClose_Flag=False;
 558  01aa 72170014      	bres	_System_Para+20,#3
 559                     ; 102 			System_Para.CloseCnt_1S=0;
 561  01ae 5f            	clrw	x
 562  01af bf10          	ldw	_System_Para+16,x
 563                     ; 104 			GPIO_WriteLow(_SYSTEM_1H_LED_PORT,_SYSTEM_1H_LED_PIN);
 565  01b1 4b10          	push	#16
 566  01b3 ae500a        	ldw	x,#20490
 567  01b6 cd0000        	call	_GPIO_WriteLow
 569  01b9 84            	pop	a
 570                     ; 105 			GPIO_WriteHigh(_SYSTEM_2H_LED_PORT,_SYSTEM_2H_LED_PIN);
 572  01ba 4b20          	push	#32
 573  01bc ae500a        	ldw	x,#20490
 574  01bf cd0000        	call	_GPIO_WriteHigh
 576  01c2 84            	pop	a
 577                     ; 106 			GPIO_WriteLow(_SYSTEM_3H_LED_PORT,_SYSTEM_3H_LED_PIN);
 579  01c3 4b40          	push	#64
 580  01c5 ae500a        	ldw	x,#20490
 581  01c8 cd0000        	call	_GPIO_WriteLow
 583  01cb 84            	pop	a
 584                     ; 107 			GPIO_WriteLow(_SYSTEM_4H_LED_PORT,_SYSTEM_4H_LED_PIN);
 586  01cc 4b80          	push	#128
 587  01ce ae500a        	ldw	x,#20490
 588  01d1 cd0000        	call	_GPIO_WriteLow
 590  01d4 84            	pop	a
 591                     ; 108 			break;
 593  01d5 2070          	jra	L77
 594  01d7               L75:
 595                     ; 112 			System_Para._1HourClose_Flag=False;
 597  01d7 72110014      	bres	_System_Para+20,#0
 598                     ; 113 			System_Para._2HourClose_Flag=False;
 600  01db 72130014      	bres	_System_Para+20,#1
 601                     ; 114 			System_Para._3HourClose_Flag=True;
 603  01df 72140014      	bset	_System_Para+20,#2
 604                     ; 115 			System_Para._4HourClose_Flag=False;
 606  01e3 72170014      	bres	_System_Para+20,#3
 607                     ; 116 			System_Para.CloseCnt_1S=0;
 609  01e7 5f            	clrw	x
 610  01e8 bf10          	ldw	_System_Para+16,x
 611                     ; 118 			GPIO_WriteLow(_SYSTEM_1H_LED_PORT,_SYSTEM_1H_LED_PIN);
 613  01ea 4b10          	push	#16
 614  01ec ae500a        	ldw	x,#20490
 615  01ef cd0000        	call	_GPIO_WriteLow
 617  01f2 84            	pop	a
 618                     ; 119 			GPIO_WriteLow(_SYSTEM_2H_LED_PORT,_SYSTEM_2H_LED_PIN);
 620  01f3 4b20          	push	#32
 621  01f5 ae500a        	ldw	x,#20490
 622  01f8 cd0000        	call	_GPIO_WriteLow
 624  01fb 84            	pop	a
 625                     ; 120 			GPIO_WriteHigh(_SYSTEM_3H_LED_PORT,_SYSTEM_3H_LED_PIN);
 627  01fc 4b40          	push	#64
 628  01fe ae500a        	ldw	x,#20490
 629  0201 cd0000        	call	_GPIO_WriteHigh
 631  0204 84            	pop	a
 632                     ; 121 			GPIO_WriteLow(_SYSTEM_4H_LED_PORT,_SYSTEM_4H_LED_PIN);
 634  0205 4b80          	push	#128
 635  0207 ae500a        	ldw	x,#20490
 636  020a cd0000        	call	_GPIO_WriteLow
 638  020d 84            	pop	a
 639                     ; 122 			break;
 641  020e 2037          	jra	L77
 642  0210               L16:
 643                     ; 126 			System_Para._1HourClose_Flag=False;
 645  0210 72110014      	bres	_System_Para+20,#0
 646                     ; 127 			System_Para._2HourClose_Flag=False;
 648  0214 72130014      	bres	_System_Para+20,#1
 649                     ; 128 			System_Para._3HourClose_Flag=False;
 651  0218 72150014      	bres	_System_Para+20,#2
 652                     ; 129 			System_Para._4HourClose_Flag=True;
 654  021c 72160014      	bset	_System_Para+20,#3
 655                     ; 130 			System_Para.CloseCnt_1S=0;
 657  0220 5f            	clrw	x
 658  0221 bf10          	ldw	_System_Para+16,x
 659                     ; 132 			GPIO_WriteLow(_SYSTEM_1H_LED_PORT,_SYSTEM_1H_LED_PIN);
 661  0223 4b10          	push	#16
 662  0225 ae500a        	ldw	x,#20490
 663  0228 cd0000        	call	_GPIO_WriteLow
 665  022b 84            	pop	a
 666                     ; 133 			GPIO_WriteLow(_SYSTEM_2H_LED_PORT,_SYSTEM_2H_LED_PIN);
 668  022c 4b20          	push	#32
 669  022e ae500a        	ldw	x,#20490
 670  0231 cd0000        	call	_GPIO_WriteLow
 672  0234 84            	pop	a
 673                     ; 134 			GPIO_WriteLow(_SYSTEM_3H_LED_PORT,_SYSTEM_3H_LED_PIN);
 675  0235 4b40          	push	#64
 676  0237 ae500a        	ldw	x,#20490
 677  023a cd0000        	call	_GPIO_WriteLow
 679  023d 84            	pop	a
 680                     ; 135 			GPIO_WriteHigh(_SYSTEM_4H_LED_PORT,_SYSTEM_4H_LED_PIN);			
 682  023e 4b80          	push	#128
 683  0240 ae500a        	ldw	x,#20490
 684  0243 cd0000        	call	_GPIO_WriteHigh
 686  0246 84            	pop	a
 687                     ; 136 			break;
 689  0247               L77:
 690                     ; 144 }
 693  0247 81            	ret
 761                     ; 146 int SYSTEM_IR_SendHandle(u16 Synccode,u16 Datacode,u16 Factorycode)
 761                     ; 147 {
 762                     	switch	.text
 763  0248               _SYSTEM_IR_SendHandle:
 765  0248 89            	pushw	x
 766  0249 88            	push	a
 767       00000001      OFST:	set	1
 770                     ; 148 	TIM2_PWM_ENABLE();
 772  024a a601          	ld	a,#1
 773  024c cd0000        	call	_TIM2_Cmd
 775                     ; 149 	SP_BEGIN(&(System_Para.sp_thd));
 777  024f a601          	ld	a,#1
 778  0251 6b01          	ld	(OFST+0,sp),a
 781  0253 b609          	ld	a,_System_Para+9
 783                     ; 242 	TIM2_PWM_DISABLE();
 784  0255 4d            	tnz	a
 785  0256 277b          	jreq	L101
 786  0258 a099          	sub	a,#153
 787  025a 2603          	jrne	L44
 788  025c cc02eb        	jp	L301
 789  025f               L44:
 790  025f a003          	sub	a,#3
 791  0261 2603          	jrne	L64
 792  0263 cc0311        	jp	L501
 793  0266               L64:
 794  0266 a009          	sub	a,#9
 795  0268 2603          	jrne	L05
 796  026a cc0350        	jp	L701
 797  026d               L05:
 798  026d a003          	sub	a,#3
 799  026f 2603          	jrne	L25
 800  0271 cc0376        	jp	L111
 801  0274               L25:
 802  0274 a006          	sub	a,#6
 803  0276 2603          	jrne	L45
 804  0278 cc039c        	jp	L311
 805  027b               L45:
 806  027b a003          	sub	a,#3
 807  027d 2603          	jrne	L65
 808  027f cc03c4        	jp	L511
 809  0282               L65:
 810  0282 a00c          	sub	a,#12
 811  0284 2603          	jrne	L06
 812  0286 cc040f        	jp	L711
 813  0289               L06:
 814  0289 a003          	sub	a,#3
 815  028b 2603          	jrne	L26
 816  028d cc0437        	jp	L121
 817  0290               L26:
 818  0290 a006          	sub	a,#6
 819  0292 2603          	jrne	L46
 820  0294 cc045f        	jp	L321
 821  0297               L46:
 822  0297 a003          	sub	a,#3
 823  0299 2603          	jrne	L66
 824  029b cc0487        	jp	L521
 825  029e               L66:
 826  029e a00c          	sub	a,#12
 827  02a0 2603          	jrne	L07
 828  02a2 cc04d2        	jp	L721
 829  02a5               L07:
 830  02a5 a003          	sub	a,#3
 831  02a7 2603          	jrne	L27
 832  02a9 cc04fa        	jp	L131
 833  02ac               L27:
 834  02ac a006          	sub	a,#6
 835  02ae 2603          	jrne	L47
 836  02b0 cc0522        	jp	L331
 837  02b3               L47:
 838  02b3 a003          	sub	a,#3
 839  02b5 2603          	jrne	L67
 840  02b7 cc054a        	jp	L531
 841  02ba               L67:
 842  02ba a006          	sub	a,#6
 843  02bc 2603          	jrne	L001
 844  02be cc057d        	jp	L731
 845  02c1               L001:
 846  02c1 a003          	sub	a,#3
 847  02c3 2603          	jrne	L201
 848  02c5 cc05a5        	jp	L141
 849  02c8               L201:
 850  02c8 a003          	sub	a,#3
 851  02ca 2603          	jrne	L401
 852  02cc cc05cd        	jp	L341
 853  02cf               L401:
 854  02cf acf205f2      	jpf	L102
 855  02d3               L101:
 856                     ; 151 	SYSTEM_IR_LOW();
 859  02d3 4b08          	push	#8
 860  02d5 ae500f        	ldw	x,#20495
 861  02d8 cd0000        	call	_GPIO_WriteLow
 863  02db 84            	pop	a
 864                     ; 152 	TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_START_LOW);
 866  02dc ae0041        	ldw	x,#65
 867  02df 89            	pushw	x
 868  02e0 ae0001        	ldw	x,#_System_Para+1
 869  02e3 cd0000        	call	_TimeOut_Record
 871  02e6 85            	popw	x
 872                     ; 153 	SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
 874  02e7 35990009      	mov	_System_Para+9,#153
 875  02eb               L301:
 879  02eb ae0001        	ldw	x,#_System_Para+1
 880  02ee cd0000        	call	_TimeOutDet_Check
 882  02f1 cd0000        	call	c_lrzmp
 884  02f4 2603          	jrne	L702
 887  02f6 5f            	clrw	x
 889  02f7 2024          	jra	L24
 890  02f9               L702:
 891                     ; 154 	SYSTEM_IR_HIGHT();
 893  02f9 4b08          	push	#8
 894  02fb ae500f        	ldw	x,#20495
 895  02fe cd0000        	call	_GPIO_WriteHigh
 897  0301 84            	pop	a
 898                     ; 155 	TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_START_HIGHT);
 900  0302 ae0050        	ldw	x,#80
 901  0305 89            	pushw	x
 902  0306 ae0001        	ldw	x,#_System_Para+1
 903  0309 cd0000        	call	_TimeOut_Record
 905  030c 85            	popw	x
 906                     ; 156 	SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
 908  030d 359c0009      	mov	_System_Para+9,#156
 909  0311               L501:
 913  0311 ae0001        	ldw	x,#_System_Para+1
 914  0314 cd0000        	call	_TimeOutDet_Check
 916  0317 cd0000        	call	c_lrzmp
 918  031a 2604          	jrne	L512
 921  031c 5f            	clrw	x
 923  031d               L24:
 925  031d 5b03          	addw	sp,#3
 926  031f 81            	ret
 927  0320               L512:
 928                     ; 158 	for(System_Para.Synccode_Cnt=0;System_Para.Synccode_Cnt<_SYSTEM_IR_DATA_NUM;System_Para.Synccode_Cnt++)
 930  0320 3f0a          	clr	_System_Para+10
 932  0322 acd603d6      	jpf	L322
 933  0326               L712:
 934                     ; 160 		Synccode=Synccode<<System_Para.Synccode_Cnt;
 936  0326 b60a          	ld	a,_System_Para+10
 937  0328 4d            	tnz	a
 938  0329 2707          	jreq	L42
 939  032b               L62:
 940  032b 0803          	sll	(OFST+2,sp)
 941  032d 0902          	rlc	(OFST+1,sp)
 942  032f 4a            	dec	a
 943  0330 26f9          	jrne	L62
 944  0332               L42:
 945                     ; 161 		if(Synccode&0x8000)
 947  0332 7b02          	ld	a,(OFST+1,sp)
 948  0334 a580          	bcp	a,#128
 949  0336 274c          	jreq	L722
 950                     ; 163 			SYSTEM_IR_LOW();
 952  0338 4b08          	push	#8
 953  033a ae500f        	ldw	x,#20495
 954  033d cd0000        	call	_GPIO_WriteLow
 956  0340 84            	pop	a
 957                     ; 164 			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
 959  0341 ae0006        	ldw	x,#6
 960  0344 89            	pushw	x
 961  0345 ae0001        	ldw	x,#_System_Para+1
 962  0348 cd0000        	call	_TimeOut_Record
 964  034b 85            	popw	x
 965                     ; 165 			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
 967  034c 35a50009      	mov	_System_Para+9,#165
 968  0350               L701:
 972  0350 ae0001        	ldw	x,#_System_Para+1
 973  0353 cd0000        	call	_TimeOutDet_Check
 975  0356 cd0000        	call	c_lrzmp
 977  0359 2603          	jrne	L532
 980  035b 5f            	clrw	x
 982  035c 20bf          	jra	L24
 983  035e               L532:
 984                     ; 166 			SYSTEM_IR_HIGHT();
 986  035e 4b08          	push	#8
 987  0360 ae500f        	ldw	x,#20495
 988  0363 cd0000        	call	_GPIO_WriteHigh
 990  0366 84            	pop	a
 991                     ; 167 			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_1);
 993  0367 ae0025        	ldw	x,#37
 994  036a 89            	pushw	x
 995  036b ae0001        	ldw	x,#_System_Para+1
 996  036e cd0000        	call	_TimeOut_Record
 998  0371 85            	popw	x
 999                     ; 168 			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1001  0372 35a80009      	mov	_System_Para+9,#168
1002  0376               L111:
1006  0376 ae0001        	ldw	x,#_System_Para+1
1007  0379 cd0000        	call	_TimeOutDet_Check
1009  037c cd0000        	call	c_lrzmp
1011  037f 2653          	jrne	L542
1014  0381 5f            	clrw	x
1016  0382 2099          	jra	L24
1017  0384               L722:
1018                     ; 172 			SYSTEM_IR_LOW();
1020  0384 4b08          	push	#8
1021  0386 ae500f        	ldw	x,#20495
1022  0389 cd0000        	call	_GPIO_WriteLow
1024  038c 84            	pop	a
1025                     ; 173 			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
1027  038d ae0006        	ldw	x,#6
1028  0390 89            	pushw	x
1029  0391 ae0001        	ldw	x,#_System_Para+1
1030  0394 cd0000        	call	_TimeOut_Record
1032  0397 85            	popw	x
1033                     ; 174 			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1035  0398 35ae0009      	mov	_System_Para+9,#174
1036  039c               L311:
1040  039c ae0001        	ldw	x,#_System_Para+1
1041  039f cd0000        	call	_TimeOutDet_Check
1043  03a2 cd0000        	call	c_lrzmp
1045  03a5 2605          	jrne	L352
1048  03a7 5f            	clrw	x
1050  03a8 ac1d031d      	jpf	L24
1051  03ac               L352:
1052                     ; 175 			SYSTEM_IR_HIGHT();
1054  03ac 4b08          	push	#8
1055  03ae ae500f        	ldw	x,#20495
1056  03b1 cd0000        	call	_GPIO_WriteHigh
1058  03b4 84            	pop	a
1059                     ; 176 			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_0);
1061  03b5 ae0010        	ldw	x,#16
1062  03b8 89            	pushw	x
1063  03b9 ae0001        	ldw	x,#_System_Para+1
1064  03bc cd0000        	call	_TimeOut_Record
1066  03bf 85            	popw	x
1067                     ; 177 			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1069  03c0 35b10009      	mov	_System_Para+9,#177
1070  03c4               L511:
1074  03c4 ae0001        	ldw	x,#_System_Para+1
1075  03c7 cd0000        	call	_TimeOutDet_Check
1077  03ca cd0000        	call	c_lrzmp
1079  03cd 2605          	jrne	L542
1082  03cf 5f            	clrw	x
1084  03d0 ac1d031d      	jpf	L24
1085  03d4               L542:
1086                     ; 158 	for(System_Para.Synccode_Cnt=0;System_Para.Synccode_Cnt<_SYSTEM_IR_DATA_NUM;System_Para.Synccode_Cnt++)
1088  03d4 3c0a          	inc	_System_Para+10
1089  03d6               L322:
1092  03d6 b60a          	ld	a,_System_Para+10
1093  03d8 a110          	cp	a,#16
1094  03da 2403          	jruge	L601
1095  03dc cc0326        	jp	L712
1096  03df               L601:
1097                     ; 182 	for(System_Para.Datacode_Cnt=0;System_Para.Datacode_Cnt<_SYSTEM_IR_DATA_NUM;System_Para.Datacode_Cnt++)
1099  03df 3f0b          	clr	_System_Para+11
1101  03e1 ac990499      	jpf	L762
1102  03e5               L362:
1103                     ; 184 		Datacode=Datacode<<System_Para.Datacode_Cnt;
1105  03e5 b60b          	ld	a,_System_Para+11
1106  03e7 4d            	tnz	a
1107  03e8 2707          	jreq	L03
1108  03ea               L23:
1109  03ea 0807          	sll	(OFST+6,sp)
1110  03ec 0906          	rlc	(OFST+5,sp)
1111  03ee 4a            	dec	a
1112  03ef 26f9          	jrne	L23
1113  03f1               L03:
1114                     ; 185 		if(Datacode&0x8000)
1116  03f1 7b06          	ld	a,(OFST+5,sp)
1117  03f3 a580          	bcp	a,#128
1118  03f5 2750          	jreq	L372
1119                     ; 187 			SYSTEM_IR_LOW();
1121  03f7 4b08          	push	#8
1122  03f9 ae500f        	ldw	x,#20495
1123  03fc cd0000        	call	_GPIO_WriteLow
1125  03ff 84            	pop	a
1126                     ; 188 			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
1128  0400 ae0006        	ldw	x,#6
1129  0403 89            	pushw	x
1130  0404 ae0001        	ldw	x,#_System_Para+1
1131  0407 cd0000        	call	_TimeOut_Record
1133  040a 85            	popw	x
1134                     ; 189 			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1136  040b 35bd0009      	mov	_System_Para+9,#189
1137  040f               L711:
1141  040f ae0001        	ldw	x,#_System_Para+1
1142  0412 cd0000        	call	_TimeOutDet_Check
1144  0415 cd0000        	call	c_lrzmp
1146  0418 2605          	jrne	L103
1149  041a 5f            	clrw	x
1151  041b ac1d031d      	jpf	L24
1152  041f               L103:
1153                     ; 190 			SYSTEM_IR_HIGHT();
1155  041f 4b08          	push	#8
1156  0421 ae500f        	ldw	x,#20495
1157  0424 cd0000        	call	_GPIO_WriteHigh
1159  0427 84            	pop	a
1160                     ; 191 			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_1);
1162  0428 ae0025        	ldw	x,#37
1163  042b 89            	pushw	x
1164  042c ae0001        	ldw	x,#_System_Para+1
1165  042f cd0000        	call	_TimeOut_Record
1167  0432 85            	popw	x
1168                     ; 192 			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1170  0433 35c00009      	mov	_System_Para+9,#192
1171  0437               L121:
1175  0437 ae0001        	ldw	x,#_System_Para+1
1176  043a cd0000        	call	_TimeOutDet_Check
1178  043d cd0000        	call	c_lrzmp
1180  0440 2655          	jrne	L113
1183  0442 5f            	clrw	x
1185  0443 ac1d031d      	jpf	L24
1186  0447               L372:
1187                     ; 196 			SYSTEM_IR_LOW();
1189  0447 4b08          	push	#8
1190  0449 ae500f        	ldw	x,#20495
1191  044c cd0000        	call	_GPIO_WriteLow
1193  044f 84            	pop	a
1194                     ; 197 			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
1196  0450 ae0006        	ldw	x,#6
1197  0453 89            	pushw	x
1198  0454 ae0001        	ldw	x,#_System_Para+1
1199  0457 cd0000        	call	_TimeOut_Record
1201  045a 85            	popw	x
1202                     ; 198 			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1204  045b 35c60009      	mov	_System_Para+9,#198
1205  045f               L321:
1209  045f ae0001        	ldw	x,#_System_Para+1
1210  0462 cd0000        	call	_TimeOutDet_Check
1212  0465 cd0000        	call	c_lrzmp
1214  0468 2605          	jrne	L713
1217  046a 5f            	clrw	x
1219  046b ac1d031d      	jpf	L24
1220  046f               L713:
1221                     ; 199 			SYSTEM_IR_HIGHT();
1223  046f 4b08          	push	#8
1224  0471 ae500f        	ldw	x,#20495
1225  0474 cd0000        	call	_GPIO_WriteHigh
1227  0477 84            	pop	a
1228                     ; 200 			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_0);
1230  0478 ae0010        	ldw	x,#16
1231  047b 89            	pushw	x
1232  047c ae0001        	ldw	x,#_System_Para+1
1233  047f cd0000        	call	_TimeOut_Record
1235  0482 85            	popw	x
1236                     ; 201 			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1238  0483 35c90009      	mov	_System_Para+9,#201
1239  0487               L521:
1243  0487 ae0001        	ldw	x,#_System_Para+1
1244  048a cd0000        	call	_TimeOutDet_Check
1246  048d cd0000        	call	c_lrzmp
1248  0490 2605          	jrne	L113
1251  0492 5f            	clrw	x
1253  0493 ac1d031d      	jpf	L24
1254  0497               L113:
1255                     ; 182 	for(System_Para.Datacode_Cnt=0;System_Para.Datacode_Cnt<_SYSTEM_IR_DATA_NUM;System_Para.Datacode_Cnt++)
1257  0497 3c0b          	inc	_System_Para+11
1258  0499               L762:
1261  0499 b60b          	ld	a,_System_Para+11
1262  049b a110          	cp	a,#16
1263  049d 2403          	jruge	L011
1264  049f cc03e5        	jp	L362
1265  04a2               L011:
1266                     ; 206 	for(System_Para.Factorycode_Cnt=0;System_Para.Factorycode_Cnt<_SYSTEM_IR_DATA_NUM;System_Para.Factorycode_Cnt++)
1268  04a2 3f0c          	clr	_System_Para+12
1270  04a4 ac5c055c      	jpf	L333
1271  04a8               L723:
1272                     ; 208 		Factorycode=Factorycode<<System_Para.Factorycode_Cnt;
1274  04a8 b60c          	ld	a,_System_Para+12
1275  04aa 4d            	tnz	a
1276  04ab 2707          	jreq	L43
1277  04ad               L63:
1278  04ad 0809          	sll	(OFST+8,sp)
1279  04af 0908          	rlc	(OFST+7,sp)
1280  04b1 4a            	dec	a
1281  04b2 26f9          	jrne	L63
1282  04b4               L43:
1283                     ; 209 		if(Factorycode&0x8000)
1285  04b4 7b08          	ld	a,(OFST+7,sp)
1286  04b6 a580          	bcp	a,#128
1287  04b8 2750          	jreq	L733
1288                     ; 211 			SYSTEM_IR_LOW();
1290  04ba 4b08          	push	#8
1291  04bc ae500f        	ldw	x,#20495
1292  04bf cd0000        	call	_GPIO_WriteLow
1294  04c2 84            	pop	a
1295                     ; 212 			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
1297  04c3 ae0006        	ldw	x,#6
1298  04c6 89            	pushw	x
1299  04c7 ae0001        	ldw	x,#_System_Para+1
1300  04ca cd0000        	call	_TimeOut_Record
1302  04cd 85            	popw	x
1303                     ; 213 			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1305  04ce 35d50009      	mov	_System_Para+9,#213
1306  04d2               L721:
1310  04d2 ae0001        	ldw	x,#_System_Para+1
1311  04d5 cd0000        	call	_TimeOutDet_Check
1313  04d8 cd0000        	call	c_lrzmp
1315  04db 2605          	jrne	L543
1318  04dd 5f            	clrw	x
1320  04de ac1d031d      	jpf	L24
1321  04e2               L543:
1322                     ; 214 			SYSTEM_IR_HIGHT();
1324  04e2 4b08          	push	#8
1325  04e4 ae500f        	ldw	x,#20495
1326  04e7 cd0000        	call	_GPIO_WriteHigh
1328  04ea 84            	pop	a
1329                     ; 215 			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_1);
1331  04eb ae0025        	ldw	x,#37
1332  04ee 89            	pushw	x
1333  04ef ae0001        	ldw	x,#_System_Para+1
1334  04f2 cd0000        	call	_TimeOut_Record
1336  04f5 85            	popw	x
1337                     ; 216 			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1339  04f6 35d80009      	mov	_System_Para+9,#216
1340  04fa               L131:
1344  04fa ae0001        	ldw	x,#_System_Para+1
1345  04fd cd0000        	call	_TimeOutDet_Check
1347  0500 cd0000        	call	c_lrzmp
1349  0503 2655          	jrne	L553
1352  0505 5f            	clrw	x
1354  0506 ac1d031d      	jpf	L24
1355  050a               L733:
1356                     ; 220 			SYSTEM_IR_LOW();
1358  050a 4b08          	push	#8
1359  050c ae500f        	ldw	x,#20495
1360  050f cd0000        	call	_GPIO_WriteLow
1362  0512 84            	pop	a
1363                     ; 221 			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
1365  0513 ae0006        	ldw	x,#6
1366  0516 89            	pushw	x
1367  0517 ae0001        	ldw	x,#_System_Para+1
1368  051a cd0000        	call	_TimeOut_Record
1370  051d 85            	popw	x
1371                     ; 222 			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1373  051e 35de0009      	mov	_System_Para+9,#222
1374  0522               L331:
1378  0522 ae0001        	ldw	x,#_System_Para+1
1379  0525 cd0000        	call	_TimeOutDet_Check
1381  0528 cd0000        	call	c_lrzmp
1383  052b 2605          	jrne	L363
1386  052d 5f            	clrw	x
1388  052e ac1d031d      	jpf	L24
1389  0532               L363:
1390                     ; 223 			SYSTEM_IR_HIGHT();
1392  0532 4b08          	push	#8
1393  0534 ae500f        	ldw	x,#20495
1394  0537 cd0000        	call	_GPIO_WriteHigh
1396  053a 84            	pop	a
1397                     ; 224 			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_0);
1399  053b ae0010        	ldw	x,#16
1400  053e 89            	pushw	x
1401  053f ae0001        	ldw	x,#_System_Para+1
1402  0542 cd0000        	call	_TimeOut_Record
1404  0545 85            	popw	x
1405                     ; 225 			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1407  0546 35e10009      	mov	_System_Para+9,#225
1408  054a               L531:
1412  054a ae0001        	ldw	x,#_System_Para+1
1413  054d cd0000        	call	_TimeOutDet_Check
1415  0550 cd0000        	call	c_lrzmp
1417  0553 2605          	jrne	L553
1420  0555 5f            	clrw	x
1422  0556 ac1d031d      	jpf	L24
1423  055a               L553:
1424                     ; 206 	for(System_Para.Factorycode_Cnt=0;System_Para.Factorycode_Cnt<_SYSTEM_IR_DATA_NUM;System_Para.Factorycode_Cnt++)
1426  055a 3c0c          	inc	_System_Para+12
1427  055c               L333:
1430  055c b60c          	ld	a,_System_Para+12
1431  055e a110          	cp	a,#16
1432  0560 2403          	jruge	L211
1433  0562 cc04a8        	jp	L723
1434  0565               L211:
1435                     ; 229 	SYSTEM_IR_LOW();
1437  0565 4b08          	push	#8
1438  0567 ae500f        	ldw	x,#20495
1439  056a cd0000        	call	_GPIO_WriteLow
1441  056d 84            	pop	a
1442                     ; 230 	TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
1444  056e ae0006        	ldw	x,#6
1445  0571 89            	pushw	x
1446  0572 ae0001        	ldw	x,#_System_Para+1
1447  0575 cd0000        	call	_TimeOut_Record
1449  0578 85            	popw	x
1450                     ; 231 	SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1452  0579 35e70009      	mov	_System_Para+9,#231
1453  057d               L731:
1457  057d ae0001        	ldw	x,#_System_Para+1
1458  0580 cd0000        	call	_TimeOutDet_Check
1460  0583 cd0000        	call	c_lrzmp
1462  0586 2605          	jrne	L773
1465  0588 5f            	clrw	x
1467  0589 ac1d031d      	jpf	L24
1468  058d               L773:
1469                     ; 232 	SYSTEM_IR_HIGHT();
1471  058d 4b08          	push	#8
1472  058f ae500f        	ldw	x,#20495
1473  0592 cd0000        	call	_GPIO_WriteHigh
1475  0595 84            	pop	a
1476                     ; 233 	TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_STOP_HIGHT);
1478  0596 ae0051        	ldw	x,#81
1479  0599 89            	pushw	x
1480  059a ae0001        	ldw	x,#_System_Para+1
1481  059d cd0000        	call	_TimeOut_Record
1483  05a0 85            	popw	x
1484                     ; 234 	SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1486  05a1 35ea0009      	mov	_System_Para+9,#234
1487  05a5               L141:
1491  05a5 ae0001        	ldw	x,#_System_Para+1
1492  05a8 cd0000        	call	_TimeOutDet_Check
1494  05ab cd0000        	call	c_lrzmp
1496  05ae 2605          	jrne	L504
1499  05b0 5f            	clrw	x
1501  05b1 ac1d031d      	jpf	L24
1502  05b5               L504:
1503                     ; 235 	SYSTEM_IR_LOW();
1505  05b5 4b08          	push	#8
1506  05b7 ae500f        	ldw	x,#20495
1507  05ba cd0000        	call	_GPIO_WriteLow
1509  05bd 84            	pop	a
1510                     ; 236 	TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
1512  05be ae0006        	ldw	x,#6
1513  05c1 89            	pushw	x
1514  05c2 ae0001        	ldw	x,#_System_Para+1
1515  05c5 cd0000        	call	_TimeOut_Record
1517  05c8 85            	popw	x
1518                     ; 237 	SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
1520  05c9 35ed0009      	mov	_System_Para+9,#237
1521  05cd               L341:
1525  05cd ae0001        	ldw	x,#_System_Para+1
1526  05d0 cd0000        	call	_TimeOutDet_Check
1528  05d3 cd0000        	call	c_lrzmp
1530  05d6 2605          	jrne	L314
1533  05d8 5f            	clrw	x
1535  05d9 ac1d031d      	jpf	L24
1536  05dd               L314:
1537                     ; 238 	SYSTEM_IR_HIGHT();
1539  05dd 4b08          	push	#8
1540  05df ae500f        	ldw	x,#20495
1541  05e2 cd0000        	call	_GPIO_WriteHigh
1543  05e5 84            	pop	a
1544                     ; 240 	System_Para.Succeed_SendFlag=True;
1546  05e6 7212000d      	bset	_System_Para+13,#1
1547                     ; 241 	System_Para.AutoCloseFlag=False;
1549  05ea 7211000f      	bres	_System_Para+15,#0
1550                     ; 242 	TIM2_PWM_DISABLE();
1552  05ee 4f            	clr	a
1553  05ef cd0000        	call	_TIM2_Cmd
1555  05f2               L102:
1556                     ; 243 	SP_END(&(System_Para.sp_thd));
1559  05f2 0f01          	clr	(OFST+0,sp)
1562  05f4 3f09          	clr	_System_Para+9
1566  05f6 ae0002        	ldw	x,#2
1568  05f9 ac1d031d      	jpf	L24
1592                     ; 248 int SYSTEM_SEND_OPEN(void)
1592                     ; 249 {
1593                     	switch	.text
1594  05fd               _SYSTEM_SEND_OPEN:
1598                     ; 250 	SYSTEM_IR_SendHandle(_SYSTEM_IR_SYNCCODE,_SYSTEM_IR_OPENCODE,_SYSTEM_IR_STOPCODE);
1600  05fd aeb847        	ldw	x,#47175
1601  0600 89            	pushw	x
1602  0601 aea15e        	ldw	x,#41310
1603  0604 89            	pushw	x
1604  0605 ae05fa        	ldw	x,#1530
1605  0608 cd0248        	call	_SYSTEM_IR_SendHandle
1607  060b 5b04          	addw	sp,#4
1608                     ; 251 }
1611  060d 81            	ret
1635                     ; 252 int SYSTEM_SEND_CLOSE(void)
1635                     ; 253 {
1636                     	switch	.text
1637  060e               _SYSTEM_SEND_CLOSE:
1641                     ; 254 	SYSTEM_IR_SendHandle(_SYSTEM_IR_SYNCCODE,_SYSTEM_IR_CLOSECODE,_SYSTEM_IR_STOPCODE);
1643  060e aeb847        	ldw	x,#47175
1644  0611 89            	pushw	x
1645  0612 aeb14e        	ldw	x,#45390
1646  0615 89            	pushw	x
1647  0616 ae05fa        	ldw	x,#1530
1648  0619 cd0248        	call	_SYSTEM_IR_SendHandle
1650  061c 5b04          	addw	sp,#4
1651                     ; 255 }
1654  061e 81            	ret
1682                     ; 256 int SYSTEM_TimeToClose_Handle(void)
1682                     ; 257 {
1683                     	switch	.text
1684  061f               _SYSTEM_TimeToClose_Handle:
1688                     ; 258 	if(System_Para.AutoCloseFlag)
1690  061f b60f          	ld	a,_System_Para+15
1691  0621 a501          	bcp	a,#1
1692  0623 2711          	jreq	L544
1693                     ; 260 		SYSTEM_SEND_CLOSE();
1695  0625 ade7          	call	_SYSTEM_SEND_CLOSE
1697                     ; 262 		System_Para.OpenFlag=False;
1699  0627 7211000d      	bres	_System_Para+13,#0
1700                     ; 263 		Key_Para.Key_ShortPressFlag=False;
1702  062b 72130002      	bres	_Key_Para+2,#1
1703                     ; 264 		System_Para.Device_Status=_SYSTEM_DEVICE_OFF;
1705  062f 3f0e          	clr	_System_Para+14
1706                     ; 265 		System_Para.System_Led_Status=SYSTEM_LED_ALL_OFF;
1708  0631 3f00          	clr	_System_Para
1709                     ; 266 		SYSTEM_LED_Handle();
1711  0633 cd0108        	call	_SYSTEM_LED_Handle
1713  0636               L544:
1714                     ; 269 }
1717  0636 81            	ret
1743                     ; 270 void SYSTEM_TimeToClose_IQRHandle(void)
1743                     ; 271 {
1744                     	switch	.text
1745  0637               _SYSTEM_TimeToClose_IQRHandle:
1749                     ; 272 	if(System_Para._4HourClose_Flag)
1751  0637 b614          	ld	a,_System_Para+20
1752  0639 a508          	bcp	a,#8
1753  063b 272e          	jreq	L754
1754                     ; 274 		if(System_Para.CloseCnt_1S++>1000)
1756  063d be10          	ldw	x,_System_Para+16
1757  063f 1c0001        	addw	x,#1
1758  0642 bf10          	ldw	_System_Para+16,x
1759  0644 1d0001        	subw	x,#1
1760  0647 a303e9        	cpw	x,#1001
1761  064a 251f          	jrult	L754
1762                     ; 276 			System_Para.CloseCnt_1S=0;
1764  064c 5f            	clrw	x
1765  064d bf10          	ldw	_System_Para+16,x
1766                     ; 277 			if(System_Para.CloseCnt++>=_SYSTEM_CLOSE_1HOUR)
1768  064f be12          	ldw	x,_System_Para+18
1769  0651 1c0001        	addw	x,#1
1770  0654 bf12          	ldw	_System_Para+18,x
1771  0656 1d0001        	subw	x,#1
1772  0659 a30e10        	cpw	x,#3600
1773  065c 250d          	jrult	L754
1774                     ; 279 				System_Para.CloseCnt=0;
1776  065e 5f            	clrw	x
1777  065f bf12          	ldw	_System_Para+18,x
1778                     ; 280 				System_Para.CloseCnt_1S=0;
1780  0661 5f            	clrw	x
1781  0662 bf10          	ldw	_System_Para+16,x
1782                     ; 281 				System_Para.System_Led_Status=SYSTEM_3H_LED_ON;
1784  0664 35030000      	mov	_System_Para,#3
1785                     ; 282 				SYSTEM_LED_Handle();
1787  0668 cd0108        	call	_SYSTEM_LED_Handle
1789  066b               L754:
1790                     ; 286 	if(System_Para._3HourClose_Flag)
1792  066b b614          	ld	a,_System_Para+20
1793  066d a504          	bcp	a,#4
1794  066f 272e          	jreq	L564
1795                     ; 288 		if(System_Para.CloseCnt_1S++>1000)
1797  0671 be10          	ldw	x,_System_Para+16
1798  0673 1c0001        	addw	x,#1
1799  0676 bf10          	ldw	_System_Para+16,x
1800  0678 1d0001        	subw	x,#1
1801  067b a303e9        	cpw	x,#1001
1802  067e 251f          	jrult	L564
1803                     ; 290 			System_Para.CloseCnt_1S=0;
1805  0680 5f            	clrw	x
1806  0681 bf10          	ldw	_System_Para+16,x
1807                     ; 291 			if(System_Para.CloseCnt++>=_SYSTEM_CLOSE_1HOUR)
1809  0683 be12          	ldw	x,_System_Para+18
1810  0685 1c0001        	addw	x,#1
1811  0688 bf12          	ldw	_System_Para+18,x
1812  068a 1d0001        	subw	x,#1
1813  068d a30e10        	cpw	x,#3600
1814  0690 250d          	jrult	L564
1815                     ; 293 				System_Para.CloseCnt=0;
1817  0692 5f            	clrw	x
1818  0693 bf12          	ldw	_System_Para+18,x
1819                     ; 294 				System_Para.CloseCnt_1S=0;
1821  0695 5f            	clrw	x
1822  0696 bf10          	ldw	_System_Para+16,x
1823                     ; 295 				System_Para.System_Led_Status=SYSTEM_2H_LED_ON;
1825  0698 35020000      	mov	_System_Para,#2
1826                     ; 296 				SYSTEM_LED_Handle();
1828  069c cd0108        	call	_SYSTEM_LED_Handle
1830  069f               L564:
1831                     ; 300 	if(System_Para._2HourClose_Flag)
1833  069f b614          	ld	a,_System_Para+20
1834  06a1 a502          	bcp	a,#2
1835  06a3 272e          	jreq	L374
1836                     ; 302 		if(System_Para.CloseCnt_1S++>1000)
1838  06a5 be10          	ldw	x,_System_Para+16
1839  06a7 1c0001        	addw	x,#1
1840  06aa bf10          	ldw	_System_Para+16,x
1841  06ac 1d0001        	subw	x,#1
1842  06af a303e9        	cpw	x,#1001
1843  06b2 251f          	jrult	L374
1844                     ; 304 			System_Para.CloseCnt_1S=0;
1846  06b4 5f            	clrw	x
1847  06b5 bf10          	ldw	_System_Para+16,x
1848                     ; 305 			if(System_Para.CloseCnt++>=_SYSTEM_CLOSE_1HOUR)
1850  06b7 be12          	ldw	x,_System_Para+18
1851  06b9 1c0001        	addw	x,#1
1852  06bc bf12          	ldw	_System_Para+18,x
1853  06be 1d0001        	subw	x,#1
1854  06c1 a30e10        	cpw	x,#3600
1855  06c4 250d          	jrult	L374
1856                     ; 307 				System_Para.CloseCnt=0;
1858  06c6 5f            	clrw	x
1859  06c7 bf12          	ldw	_System_Para+18,x
1860                     ; 308 				System_Para.CloseCnt_1S=0;
1862  06c9 5f            	clrw	x
1863  06ca bf10          	ldw	_System_Para+16,x
1864                     ; 309 				System_Para.System_Led_Status=SYSTEM_1H_LED_ON;
1866  06cc 35010000      	mov	_System_Para,#1
1867                     ; 310 				SYSTEM_LED_Handle();
1869  06d0 cd0108        	call	_SYSTEM_LED_Handle
1871  06d3               L374:
1872                     ; 314 	if(System_Para._1HourClose_Flag)
1874  06d3 b614          	ld	a,_System_Para+20
1875  06d5 a501          	bcp	a,#1
1876  06d7 2730          	jreq	L105
1877                     ; 316 		if(System_Para.CloseCnt_1S++>1000)
1879  06d9 be10          	ldw	x,_System_Para+16
1880  06db 1c0001        	addw	x,#1
1881  06de bf10          	ldw	_System_Para+16,x
1882  06e0 1d0001        	subw	x,#1
1883  06e3 a303e9        	cpw	x,#1001
1884  06e6 2521          	jrult	L105
1885                     ; 318 			System_Para.CloseCnt_1S=0;
1887  06e8 5f            	clrw	x
1888  06e9 bf10          	ldw	_System_Para+16,x
1889                     ; 319 			if(System_Para.CloseCnt++>=_SYSTEM_CLOSE_1HOUR)
1891  06eb be12          	ldw	x,_System_Para+18
1892  06ed 1c0001        	addw	x,#1
1893  06f0 bf12          	ldw	_System_Para+18,x
1894  06f2 1d0001        	subw	x,#1
1895  06f5 a30e10        	cpw	x,#3600
1896  06f8 250f          	jrult	L105
1897                     ; 321 				System_Para.CloseCnt=0;
1899  06fa 5f            	clrw	x
1900  06fb bf12          	ldw	_System_Para+18,x
1901                     ; 322 				System_Para.CloseCnt_1S=0;
1903  06fd 5f            	clrw	x
1904  06fe bf10          	ldw	_System_Para+16,x
1905                     ; 323 				System_Para.System_Led_Status=SYSTEM_LED_ALL_OFF;
1907  0700 3f00          	clr	_System_Para
1908                     ; 324 				SYSTEM_LED_Handle();
1910  0702 cd0108        	call	_SYSTEM_LED_Handle
1912                     ; 325 				System_Para.AutoCloseFlag=True;
1914  0705 7210000f      	bset	_System_Para+15,#0
1915  0709               L105:
1916                     ; 331 }
1919  0709 81            	ret
1948                     ; 332 void SYSTEM_Handle(void)
1948                     ; 333 {
1949                     	switch	.text
1950  070a               _SYSTEM_Handle:
1954                     ; 334 	SYSTEM_TimeToClose_Handle();
1956  070a cd061f        	call	_SYSTEM_TimeToClose_Handle
1958                     ; 336 	if(Key_Para.Key_ShortPressFlag)
1960  070d b602          	ld	a,_Key_Para+2
1961  070f a502          	bcp	a,#2
1962  0711 270c          	jreq	L715
1963                     ; 338 		System_Para.OpenFlag=~System_Para.OpenFlag;
1965  0713 9010000d      	bcpl	_System_Para+13,#0
1966                     ; 339 		System_Para.Succeed_SendFlag=False;
1968  0717 7213000d      	bres	_System_Para+13,#1
1969                     ; 340 		Key_Para.Key_ShortPressFlag=False;
1971  071b 72130002      	bres	_Key_Para+2,#1
1972  071f               L715:
1973                     ; 342 	if(System_Para.OpenFlag)
1975  071f b60d          	ld	a,_System_Para+13
1976  0721 a501          	bcp	a,#1
1977  0723 270f          	jreq	L125
1978                     ; 344 		if(System_Para.Succeed_SendFlag==False)
1980  0725 b60d          	ld	a,_System_Para+13
1981  0727 a502          	bcp	a,#2
1982  0729 2614          	jrne	L525
1983                     ; 346 			SYSTEM_SEND_OPEN();
1985  072b cd05fd        	call	_SYSTEM_SEND_OPEN
1987                     ; 347 			System_Para.Device_Status=_SYSTEM_DEVICE_ON; // �յ�����״̬ 
1989  072e 3501000e      	mov	_System_Para+14,#1
1990  0732 200b          	jra	L525
1991  0734               L125:
1992                     ; 352 		if(System_Para.Succeed_SendFlag==False)
1994  0734 b60d          	ld	a,_System_Para+13
1995  0736 a502          	bcp	a,#2
1996  0738 2605          	jrne	L525
1997                     ; 354 			SYSTEM_SEND_CLOSE();
1999  073a cd060e        	call	_SYSTEM_SEND_CLOSE
2001                     ; 355 			System_Para.Device_Status=_SYSTEM_DEVICE_OFF;
2003  073d 3f0e          	clr	_System_Para+14
2004  073f               L525:
2005                     ; 359 	if(System_Para.Device_Status==_SYSTEM_DEVICE_ON)
2007  073f b60e          	ld	a,_System_Para+14
2008  0741 a101          	cp	a,#1
2009  0743 2619          	jrne	L135
2010                     ; 361 		if(Key_Para.Key_DoublePressFlag)
2012  0745 b606          	ld	a,_Key_Para+6
2013  0747 a501          	bcp	a,#1
2014  0749 2722          	jreq	L735
2015                     ; 363 			Key_Para.Key_DoublePressFlag=False;
2017  074b 72110006      	bres	_Key_Para+6,#0
2018                     ; 364 			if(++System_Para.System_Led_Status==SYSTEM_USELESS)
2020  074f 3c00          	inc	_System_Para
2021  0751 b600          	ld	a,_System_Para
2022  0753 a105          	cp	a,#5
2023  0755 2602          	jrne	L535
2024                     ; 366 				System_Para.System_Led_Status=SYSTEM_LED_ALL_OFF;
2026  0757 3f00          	clr	_System_Para
2027  0759               L535:
2028                     ; 368 			SYSTEM_LED_Handle();
2030  0759 cd0108        	call	_SYSTEM_LED_Handle
2032  075c 200f          	jra	L735
2033  075e               L135:
2034                     ; 373 		System_Para.System_Led_Status=SYSTEM_LED_ALL_OFF;
2036  075e 3f00          	clr	_System_Para
2037                     ; 374 		SYSTEM_LED_Handle();
2039  0760 cd0108        	call	_SYSTEM_LED_Handle
2041                     ; 375 		if(Key_Para.Key_DoublePressFlag)
2043  0763 b606          	ld	a,_Key_Para+6
2044  0765 a501          	bcp	a,#1
2045  0767 2704          	jreq	L735
2046                     ; 377 			Key_Para.Key_DoublePressFlag=False;
2048  0769 72110006      	bres	_Key_Para+6,#0
2049  076d               L735:
2050                     ; 380 }
2053  076d 81            	ret
2322                     	xdef	_SYSTEM_TIM4_Init
2323                     	xdef	_SYSTEM_TIM1_Init
2324                     	xref.b	_Key_Para
2325                     	switch	.ubsct
2326  0000               _System_Para:
2327  0000 000000000000  	ds.b	21
2328                     	xdef	_System_Para
2329                     	xdef	_SYSTEM_Handle
2330                     	xdef	_SYSTEM_TimeToClose_IQRHandle
2331                     	xdef	_SYSTEM_TimeToClose_Handle
2332                     	xdef	_SYSTEM_SEND_CLOSE
2333                     	xdef	_SYSTEM_SEND_OPEN
2334                     	xdef	_SYSTEM_IR_SendHandle
2335                     	xdef	_SYSTEM_TIM2_PWM_Init
2336                     	xdef	_SYSTEM_LED_Handle
2337                     	xdef	_SYSTEM_Function_Init
2338                     	xref	_TimeOutDet_Check
2339                     	xref	_TimeOut_Record
2340                     	xref	_TimeOutDet_Init
2341                     	xref	_TIM4_ARRPreloadConfig
2342                     	xref	_TIM4_PrescalerConfig
2343                     	xref	_TIM4_UpdateRequestConfig
2344                     	xref	_TIM4_UpdateDisableConfig
2345                     	xref	_TIM4_ITConfig
2346                     	xref	_TIM4_Cmd
2347                     	xref	_TIM4_TimeBaseInit
2348                     	xref	_TIM4_DeInit
2349                     	xref	_TIM2_OC3PreloadConfig
2350                     	xref	_TIM2_ARRPreloadConfig
2351                     	xref	_TIM2_Cmd
2352                     	xref	_TIM2_OC3Init
2353                     	xref	_TIM2_TimeBaseInit
2354                     	xref	_TIM1_UpdateRequestConfig
2355                     	xref	_TIM1_UpdateDisableConfig
2356                     	xref	_TIM1_ITConfig
2357                     	xref	_TIM1_Cmd
2358                     	xref	_TIM1_TimeBaseInit
2359                     	xref	_TIM1_DeInit
2360                     	xref	_GPIO_WriteLow
2361                     	xref	_GPIO_WriteHigh
2362                     	xref	_GPIO_Init
2363                     	xref	_CLK_SYSCLKConfig
2383                     	xref	c_lrzmp
2384                     	end
