   1                     ; C Compiler for STM8 (COSMIC Software)
   2                     ; Generator V4.2.4 - 19 Dec 2007
  35                     ; 20  void Key_Scan(void)
  35                     ; 21  {
  37                     	switch	.text
  38  0000               _Key_Scan:
  42                     ; 23 		switch(Key_Para.Key_Status)
  44  0000 b600          	ld	a,_Key_Para
  46                     ; 122 				break;
  47  0002 4d            	tnz	a
  48  0003 270d          	jreq	L3
  49  0005 4a            	dec	a
  50  0006 2721          	jreq	L5
  51  0008 4a            	dec	a
  52  0009 2741          	jreq	L7
  53  000b 4a            	dec	a
  54  000c 2755          	jreq	L11
  55  000e               L31:
  56                     ; 121 				Key_Para.Key_Status=KeyPress;//system ununsual handler
  58  000e 3f00          	clr	_Key_Para
  59                     ; 122 				break;
  61  0010 207b          	jra	L53
  62  0012               L3:
  63                     ; 27 				if((KEY==Key_VaildValue))//decedet Key & extern power has plug in
  65  0012 4b08          	push	#8
  66  0014 ae500a        	ldw	x,#20490
  67  0017 cd0000        	call	_GPIO_ReadInputPin
  69  001a 5b01          	addw	sp,#1
  70  001c 4d            	tnz	a
  71  001d 266e          	jrne	L53
  72                     ; 29 					Key_Para.Key_Status=PressDelay;//goto next process
  74  001f 35010000      	mov	_Key_Para,#1
  75                     ; 30 					Key_Para.Key_Delay=1;//start delay
  77  0023 35010001      	mov	_Key_Para+1,#1
  78  0027 2064          	jra	L53
  79  0029               L5:
  80                     ; 36 				if(Key_Para.Key_Delay>KeyPressDelayValue)//delay 20ms
  82  0029 b601          	ld	a,_Key_Para+1
  83  002b a133          	cp	a,#51
  84  002d 255e          	jrult	L53
  85                     ; 38 					if((KEY==Key_VaildValue))
  87  002f 4b08          	push	#8
  88  0031 ae500a        	ldw	x,#20490
  89  0034 cd0000        	call	_GPIO_ReadInputPin
  91  0037 5b01          	addw	sp,#1
  92  0039 4d            	tnz	a
  93  003a 260a          	jrne	L34
  94                     ; 40 						Key_Para.Key_Status= KeyRelease;
  96  003c 35020000      	mov	_Key_Para,#2
  97                     ; 41 						Key_Para.Key_PressEN=True;//按键按下使能标志
  99  0040 72140002      	bset	_Key_Para+2,#2
 101  0044 2047          	jra	L53
 102  0046               L34:
 103                     ; 48 						Key_Para.Key_Status=KeyPress;//create a shake return initlize status
 105  0046 3f00          	clr	_Key_Para
 106                     ; 49 						Key_Para.Key_Delay=0;
 108  0048 3f01          	clr	_Key_Para+1
 109  004a 2041          	jra	L53
 110  004c               L7:
 111                     ; 60 				if(KEY)
 113  004c 4b08          	push	#8
 114  004e ae500a        	ldw	x,#20490
 115  0051 cd0000        	call	_GPIO_ReadInputPin
 117  0054 5b01          	addw	sp,#1
 118  0056 4d            	tnz	a
 119  0057 2734          	jreq	L53
 120                     ; 62 					Key_Para.Key_Status=ReleaseDelay;
 122  0059 35030000      	mov	_Key_Para,#3
 123                     ; 63 					Key_Para.Key_Delay=1;
 125  005d 35010001      	mov	_Key_Para+1,#1
 126  0061 202a          	jra	L53
 127  0063               L11:
 128                     ; 69 				if(Key_Para.Key_Delay>KeyPressDelayValue)//delay 20ms prevent shake
 130  0063 b601          	ld	a,_Key_Para+1
 131  0065 a133          	cp	a,#51
 132  0067 2524          	jrult	L53
 133                     ; 71 					if(KEY)
 135  0069 4b08          	push	#8
 136  006b ae500a        	ldw	x,#20490
 137  006e cd0000        	call	_GPIO_ReadInputPin
 139  0071 5b01          	addw	sp,#1
 140  0073 4d            	tnz	a
 141  0074 2713          	jreq	L35
 142                     ; 73 						Key_Para.Key_Status=KeyPress;//goto initliize status
 144  0076 3f00          	clr	_Key_Para
 145                     ; 74 						Key_Para.Key_Delay=0;
 147  0078 3f01          	clr	_Key_Para+1
 148                     ; 100 								if(Key_Para.Key_DoublePressNum==0)
 150  007a 3d05          	tnz	_Key_Para+5
 151  007c 2605          	jrne	L55
 152                     ; 102 									Key_Para.Key_DoublePressCnt=1;//start double press count 
 154  007e ae0001        	ldw	x,#1
 155  0081 bf03          	ldw	_Key_Para+3,x
 156  0083               L55:
 157                     ; 104 								Key_Para.Key_SinglePressComplate=True;
 159  0083 72100002      	bset	_Key_Para+2,#0
 161  0087 2004          	jra	L53
 162  0089               L35:
 163                     ; 114 						Key_Para.Key_Status=KeyRelease;
 165  0089 35020000      	mov	_Key_Para,#2
 166  008d               L53:
 167                     ; 126 			Key_Handler();
 169  008d ad01          	call	_Key_Handler
 171                     ; 128  }
 174  008f 81            	ret
 198                     ; 137 void Key_Handler(void)
 198                     ; 138 {
 199                     	switch	.text
 200  0090               _Key_Handler:
 204                     ; 140 		if(Key_Para.Key_DoublePressCnt>0&&Key_Para.Key_DoublePressCnt<KeyDoublePressDelayValue)
 206  0090 be03          	ldw	x,_Key_Para+3
 207  0092 2726          	jreq	L17
 209  0094 be03          	ldw	x,_Key_Para+3
 210  0096 a303e8        	cpw	x,#1000
 211  0099 241f          	jruge	L17
 212                     ; 142 			if(Key_Para.Key_SinglePressComplate==True)	
 214  009b b602          	ld	a,_Key_Para+2
 215  009d a501          	bcp	a,#1
 216  009f 2719          	jreq	L17
 217                     ; 144 				Key_Para.Key_SinglePressComplate=False;
 219  00a1 72110002      	bres	_Key_Para+2,#0
 220                     ; 145 				if(++Key_Para.Key_DoublePressNum==2)
 222  00a5 3c05          	inc	_Key_Para+5
 223  00a7 b605          	ld	a,_Key_Para+5
 224  00a9 a102          	cp	a,#2
 225  00ab 260d          	jrne	L17
 226                     ; 147 					Key_Para.Key_ShortPressFlag=False;
 228  00ad 72130002      	bres	_Key_Para+2,#1
 229                     ; 148 					Key_Para.Key_DoublePressFlag=True;//双击按键一次标记
 231  00b1 72100006      	bset	_Key_Para+6,#0
 232                     ; 149 					Key_Para.Key_DoublePressCnt=0;
 234  00b5 5f            	clrw	x
 235  00b6 bf03          	ldw	_Key_Para+3,x
 236                     ; 150 					Key_Para.Key_DoublePressNum=0;
 238  00b8 3f05          	clr	_Key_Para+5
 239  00ba               L17:
 240                     ; 154 		if(Key_Para.Key_DoublePressCnt>KeyDoublePressDelayValue)//double press timeout 
 242  00ba be03          	ldw	x,_Key_Para+3
 243  00bc a303e9        	cpw	x,#1001
 244  00bf 250d          	jrult	L77
 245                     ; 156 			Key_Para.Key_ShortPressFlag=True;//短按按键一次标记
 247  00c1 72120002      	bset	_Key_Para+2,#1
 248                     ; 157 			Key_Para.Key_DoublePressFlag=False;
 250  00c5 72110006      	bres	_Key_Para+6,#0
 251                     ; 158 			Key_Para.Key_DoublePressCnt=0;
 253  00c9 5f            	clrw	x
 254  00ca bf03          	ldw	_Key_Para+3,x
 255                     ; 159 			Key_Para.Key_DoublePressNum=0;
 257  00cc 3f05          	clr	_Key_Para+5
 258  00ce               L77:
 259                     ; 162 }
 262  00ce 81            	ret
 287                     ; 175 void Key_InterruptHandler(void)
 287                     ; 176 {
 288                     	switch	.text
 289  00cf               _Key_InterruptHandler:
 293                     ; 177 	if(Key_Para.Key_Delay>0&&Key_Para.Key_Delay<255)
 295  00cf 3d01          	tnz	_Key_Para+1
 296  00d1 2708          	jreq	L111
 298  00d3 b601          	ld	a,_Key_Para+1
 299  00d5 a1ff          	cp	a,#255
 300  00d7 2402          	jruge	L111
 301                     ; 179 		Key_Para.Key_Delay++;
 303  00d9 3c01          	inc	_Key_Para+1
 304  00db               L111:
 305                     ; 193 		if((Key_Para.Key_DoublePressCnt>0)&&(Key_Para.Key_DoublePressCnt<4000))
 307  00db be03          	ldw	x,_Key_Para+3
 308  00dd 270e          	jreq	L311
 310  00df be03          	ldw	x,_Key_Para+3
 311  00e1 a30fa0        	cpw	x,#4000
 312  00e4 2407          	jruge	L311
 313                     ; 195 			Key_Para.Key_DoublePressCnt++;
 315  00e6 be03          	ldw	x,_Key_Para+3
 316  00e8 1c0001        	addw	x,#1
 317  00eb bf03          	ldw	_Key_Para+3,x
 318  00ed               L311:
 319                     ; 198 }
 322  00ed 81            	ret
 467                     	switch	.ubsct
 468  0000               _Key_Para:
 469  0000 000000000000  	ds.b	7
 470                     	xdef	_Key_Para
 471                     	xdef	_Key_InterruptHandler
 472                     	xdef	_Key_Handler
 473                     	xdef	_Key_Scan
 474                     	xref	_GPIO_ReadInputPin
 494                     	end
