/* MAIN.C file
 * 
 * Copyright (c) 2002-2005 STMicroelectronics
 */



#include "stm8s_conf.h"

/************************************************/
KEY_EXT KEY_PARA Key_Para;
extern SYSTEM_PARA System_Para;

/***********************************************/

void main(void)
{
	SYSTEM_Function_Init();

	enableInterrupts();


	
	
	while (1)
	{
		Key_Scan();
		SYSTEM_Handle();
		
	}
}