/*	
	说明:单按键短按，长按，双击函数库
	注意事项:在使用此函数时，对应的定时器中断是1ms一次
	日期:2012-11-14
	版本:VER1.0
*/

#ifndef _KEY_H_
#define _KEY_H_
#include "stm8s_conf.h"


/***********Extern Globals Defined*************************/

#ifdef KEY_GLOBALS
#define KEY_EXT 
#else
#define KEY_EXT extern
#endif



/**********************************************/
#define KEY_PORT GPIOC
#define KEY_PIN GPIO_PIN_3
#define KEY_Init() GPIO_Init(KEY_PORT,KEY_PIN,GPIO_MODE_IN_PU_NO_IT)
					
#define KEY GPIO_ReadInputPin(KEY_PORT,KEY_PIN)


#define Key_VaildValue				0
#define Key_InVaildValue			1


#define Key_ExternCode					0
#define LongPress_Function_Enable		0//长按功能使能位
#define DoublePress_Function_Enable	1//双击功能使能位
#define ShortPress_Fuction_Enable		1//短按功能使能位
/*********************************************/
#define KeyPressDelayValue				50
#define KeyLongPressDelayValue			2000//单位ms
#define KeyDoublePressDelayValue			1000//单位ms
#define LightFlashDelayValue				100//单位ms
/***********************按键参数定义***********************/
typedef enum
{
	KeyPress=0,
	PressDelay,
	KeyRelease,
	ReleaseDelay
}
KEY_STATUS;

typedef struct
{
	volatile KEY_STATUS 	Key_Status;
	volatile unsigned char 	Key_Delay;
	#if ShortPress_Fuction_Enable
	volatile unsigned char 	Key_SinglePressComplate:1;
	volatile unsigned char	Key_ShortPressFlag:1;
	volatile unsigned char 	Key_PressEN:1;
	#endif
	#if LongPress_Function_Enable
	volatile unsigned int 	Key_LongPressCnt;
	volatile unsigned char	Key_LongPressFlag:1;
	volatile unsigned char 	Key_LongPressHandler:1;
	#endif
	#if DoublePress_Function_Enable
	volatile unsigned int	Key_DoublePressCnt;
	volatile unsigned char 	Key_DoublePressNum;
	volatile unsigned char 	Key_DoublePressFlag:1;
	#endif
	
}
KEY_PARA; 
/*************************按键参数定义完成******************************/



/**********************************************************************/
 void Key_Scan(void);
void Key_Handler(void);
void Key_InterruptHandler(void);



#endif
