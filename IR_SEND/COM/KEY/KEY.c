#define KEY_GLOBALS

#include "stm8s_conf.h"
#include "key.h"

/************Globals Defined***********************************/
KEY_EXT KEY_PARA Key_Para;

/**********************************************/

void Key_Scan(void);
void Key_Handler(void);

  /*
Function Name:Key_Scan
Function :Key Scan 
Parameter:none
Return value:none
 */
 void Key_Scan(void)
 {
 	
		switch(Key_Para.Key_Status)
		{
			case KeyPress:
			{
				if((KEY==Key_VaildValue))//decedet Key & extern power has plug in
				{	
					Key_Para.Key_Status=PressDelay;//goto next process
					Key_Para.Key_Delay=1;//start delay
				}
				break;
			}
			case PressDelay:
			{
				if(Key_Para.Key_Delay>KeyPressDelayValue)//delay 20ms
				{
					if((KEY==Key_VaildValue))
					{
						Key_Para.Key_Status= KeyRelease;
						Key_Para.Key_PressEN=True;//按键按下使能标志
						#if LongPress_Function_Enable==True
							Key_Para.Key_LongPressCnt=1;//long press flag enable
						#endif
					}
					else		
					{
						Key_Para.Key_Status=KeyPress;//create a shake return initlize status
						Key_Para.Key_Delay=0;
						#if LongPress_Function_Enable==True
							Key_Para.Key_LongPressCnt=0;//long press flag enable
						#endif
						/*******************************************************/
					}
				}
				break;
			}
			case KeyRelease:
			{
				if(KEY)
				{
					Key_Para.Key_Status=ReleaseDelay;
					Key_Para.Key_Delay=1;
				}
				break;	
			}
			case ReleaseDelay:
			{
				if(Key_Para.Key_Delay>KeyPressDelayValue)//delay 20ms prevent shake
				{
					if(KEY)
					{	
						Key_Para.Key_Status=KeyPress;//goto initliize status
						Key_Para.Key_Delay=0;
						#if LongPress_Function_Enable==True
							if(Key_Para.Key_LongPressFlag==False)
							{
								#if DoublePress_Function_Enable==True
									if(Key_Para.Key_DoublePressNum==0)
									{
										Key_Para.Key_DoublePressCnt=1;//start double press count 
									}
									Key_Para.Key_SinglePressComplate=True;
								#else
									#if ShortPress_Fuction_Enable==True
										Key_Para.Key_ShortPressFlag=True;	
									#endif
								#endif
									Key_Para.Key_LongPressCnt=0;//clear long press count
							}
							else
							{
								#if DoublePress_Function_Enable==True
									Key_Para.Key_DoublePressCnt=0;
								#endif
								Key_Para.Key_LongPressFlag=False;
							}
						#else
							#if DoublePress_Function_Enable==True
								if(Key_Para.Key_DoublePressNum==0)
								{
									Key_Para.Key_DoublePressCnt=1;//start double press count 
								}
								Key_Para.Key_SinglePressComplate=True;
							#else
								#if ShortPress_Fuction_Enable==True
										Key_Para.Key_ShortPressFlag=True;	
								#endif
							#endif
						#endif
					}
					else
					{
						Key_Para.Key_Status=KeyRelease;
					}
				}
				break;
			}
			default:
			{
				Key_Para.Key_Status=KeyPress;//system ununsual handler
				break;
			}
		}
		#if DoublePress_Function_Enable
			Key_Handler();
		#endif
 }

  /*
Function Name:Key_Handler
Function:key status handler
Parameter:none
Return value:none
说明:双击按键的识别处理 ，可以再主循环中使用，也可加在 Key_Scan()函数中加入
*/
void Key_Handler(void)
{
	#if DoublePress_Function_Enable
		if(Key_Para.Key_DoublePressCnt>0&&Key_Para.Key_DoublePressCnt<KeyDoublePressDelayValue)
		{
			if(Key_Para.Key_SinglePressComplate==True)	
			{
				Key_Para.Key_SinglePressComplate=False;
				if(++Key_Para.Key_DoublePressNum==2)
				{
					Key_Para.Key_ShortPressFlag=False;
					Key_Para.Key_DoublePressFlag=True;//双击按键一次标记
					Key_Para.Key_DoublePressCnt=0;
					Key_Para.Key_DoublePressNum=0;
				}
			}
		}
		if(Key_Para.Key_DoublePressCnt>KeyDoublePressDelayValue)//double press timeout 
		{
			Key_Para.Key_ShortPressFlag=True;//短按按键一次标记
			Key_Para.Key_DoublePressFlag=False;
			Key_Para.Key_DoublePressCnt=0;
			Key_Para.Key_DoublePressNum=0;
		}
	#endif
}





/*
Function Name:Key_InterruptHandler
Function:add this function to Timer interrupt Handler function
Parameter:none
return value:none
说明;此函数用于定时器的中断服务函数中。用于相关计时的处理
*/
void Key_InterruptHandler(void)
{
	if(Key_Para.Key_Delay>0&&Key_Para.Key_Delay<255)
	{
		Key_Para.Key_Delay++;
	}
	#if LongPress_Function_Enable
		if((Key_Para.Key_LongPressCnt>0)&&(Key_Para.Key_LongPressCnt<4000))
		{
			if(Key_Para.Key_LongPressCnt++>KeyLongPressDelayValue)
			{
				Key_Para.Key_LongPressCnt=0;
				Key_Para.Key_LongPressFlag=True;
				Key_Para.Key_LongPressHandler=True;
			}
		}
	#endif
	#if DoublePress_Function_Enable
		if((Key_Para.Key_DoublePressCnt>0)&&(Key_Para.Key_DoublePressCnt<4000))
		{
			Key_Para.Key_DoublePressCnt++;
		}
	#endif
}


/****************************************************************************************************************/

		