

#ifndef __SYSTEM_H
#define __SYSTEM_H

/********************************************/
#include "timeout.h"
#include "sample_thread.h"

/*******************************************/

#define True  1
#define False 0

#define _SYSTEM_DEVICE_ON 1
#define _SYSTEM_DEVICE_OFF 0


#define TIM1_Prescaler_8            7
#define Tim1_Period_10MS            10000
#define Tim1_Period_1MS             1000
#define TIM4_Delay100us				100
#define TIM4_Delay20us              20
#define TIM4_Delay90us				90

#define TIM2_PWM_ENABLE() TIM2_Cmd(ENABLE)
#define TIM2_PWM_DISABLE() TIM2_Cmd(DISABLE)

#define _SYSTEM_IR_DATA_NUM     16
#define _SYSTEM_IR_START_HIGHT  80 
#define _SYSTEM_IR_START_LOW    65 
#define _SYSTEM_IR_DATA_LOW     6
#define _SYSTEM_IR_DATA_0       16
#define _SYSTEM_IR_DATA_1       37
#define _SYSTEM_IR_STOP_HIGHT   81
#define _SYSTEM_IR_TABALE_NUM   3

#define _SYSTEM_IR_SYNCCODE  0x05fa
#define _SYSTEM_IR_OPENCODE  0xA15E
#define _SYSTEM_IR_CLOSECODE 0XB14E
#define _SYSTEM_IR_STOPCODE  0XB847

#define _SYSTEM_CLOSE_1HOUR  3600
#define _SYSTEM_CLOSE_2HOUR
#define _SYSTEM_CLOSE_3HOUR
#define _SYSTEM_CLOSE_4HOUR

#define _SYSTEM_1H_LED_PORT GPIOC
#define _SYSTEM_1H_LED_PIN GPIO_PIN_4

#define _SYSTEM_2H_LED_PORT GPIOC
#define _SYSTEM_2H_LED_PIN GPIO_PIN_5

#define _SYSTEM_3H_LED_PORT GPIOC
#define _SYSTEM_3H_LED_PIN GPIO_PIN_6

#define _SYSTEM_4H_LED_PORT GPIOC
#define _SYSTEM_4H_LED_PIN GPIO_PIN_7

#define _SYSTEM_IR_PORT GPIOD
#define _SYSTEM_IR_PIN  GPIO_PIN_3
#define SYSTEM_Led_Init()  {\
							GPIO_Init(_SYSTEM_1H_LED_PORT,_SYSTEM_1H_LED_PIN,GPIO_MODE_OUT_PP_HIGH_FAST);\
							GPIO_Init(_SYSTEM_2H_LED_PORT,_SYSTEM_2H_LED_PIN,GPIO_MODE_OUT_PP_HIGH_FAST);\
							GPIO_Init(_SYSTEM_3H_LED_PORT,_SYSTEM_3H_LED_PIN,GPIO_MODE_OUT_PP_HIGH_FAST);\
							GPIO_Init(_SYSTEM_4H_LED_PORT,_SYSTEM_4H_LED_PIN,GPIO_MODE_OUT_PP_HIGH_FAST);\
							GPIO_WriteLow(_SYSTEM_1H_LED_PORT,_SYSTEM_1H_LED_PIN);\
							GPIO_WriteLow(_SYSTEM_2H_LED_PORT,_SYSTEM_2H_LED_PIN);\
							GPIO_WriteLow(_SYSTEM_3H_LED_PORT,_SYSTEM_3H_LED_PIN);\
							GPIO_WriteLow(_SYSTEM_4H_LED_PORT,_SYSTEM_4H_LED_PIN);}

#define SYSTEM_IR_Init() {\
							GPIO_Init(_SYSTEM_IR_PORT,_SYSTEM_IR_PIN,GPIO_MODE_OUT_PP_LOW_FAST);\
							GPIO_WriteLow(_SYSTEM_IR_PORT,_SYSTEM_IR_PIN);}
#define SYSTEM_IR_HIGHT() GPIO_WriteHigh(_SYSTEM_IR_PORT,_SYSTEM_IR_PIN)
#define SYSTEM_IR_LOW() GPIO_WriteLow(_SYSTEM_IR_PORT,_SYSTEM_IR_PIN)


typedef enum
{
	SYSTEM_LED_ALL_OFF=0,
	SYSTEM_1H_LED_ON,
	SYSTEM_2H_LED_ON,
	SYSTEM_3H_LED_ON,
	SYSTEM_4H_LED_ON,
	SYSTEM_USELESS
}
SYSTEM_LED_STATUS;


typedef struct
{
	__IO SYSTEM_LED_STATUS System_Led_Status;
	
	TIMEOUT_PARA timer;
	spthd sp_thd;
	__IO u8 Synccode_Cnt;
	__IO u8 Datacode_Cnt;
	__IO u8 Factorycode_Cnt;
	__IO u8 OpenFlag:1;
	__IO u8 Succeed_SendFlag:1;
	__IO u8 Device_Status;

	__IO u8 AutoCloseFlag:1;
	
	__IO u16 CloseCnt_1S;
	__IO u16 CloseCnt;
	__IO u8 _1HourClose_Flag:1;
	__IO u8 _2HourClose_Flag:1;
	__IO u8 _3HourClose_Flag:1;
	__IO u8 _4HourClose_Flag:1;
}
SYSTEM_PARA;














void SYSTEM_Function_Init(void);
void SYSTEM_LED_Handle(void);
void SYSTEM_TIM2_PWM_Init(void);
int SYSTEM_IR_SendHandle(u16 Synccode,u16 Datacode,u16 Factorycode);
int SYSTEM_SEND_OPEN(void);
int SYSTEM_SEND_CLOSE(void);
int SYSTEM_TimeToClose_Handle(void);
void SYSTEM_TimeToClose_IQRHandle(void);
void SYSTEM_Handle(void);












#endif
