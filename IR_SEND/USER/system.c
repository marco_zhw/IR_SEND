#include "stm8s_conf.h"
#include "key.h"

/***************************************************/
SYSTEM_PARA System_Para;
KEY_EXT KEY_PARA Key_Para;

/**************************************************/

void SYSTEM_TIM1_Init(void)
{
	TIM1_DeInit();
	TIM1_TimeBaseInit(TIM1_Prescaler_8,TIM2_PSCRELOADMODE_UPDATE,Tim1_Period_1MS,0);   //1ms
	TIM1_ITConfig(TIM1_IT_UPDATE,ENABLE);
	TIM1_UpdateDisableConfig(DISABLE);
	TIM1_UpdateRequestConfig( TIM1_UPDATESOURCE_GLOBAL);
	TIM1_Cmd(ENABLE);
}

void SYSTEM_TIM4_Init(void)
{
	TIM4_DeInit();
	TIM4_TimeBaseInit( TIM4_PRESCALER_8 ,TIM4_Delay90us-2);///90us
	TIM4_PrescalerConfig(TIM4_PRESCALER_8,TIM4_PSCRELOADMODE_IMMEDIATE);
	TIM4_ARRPreloadConfig(ENABLE);
	TIM4_ITConfig( TIM4_IT_UPDATE , ENABLE);
	TIM4_UpdateDisableConfig(DISABLE);
	TIM4_UpdateRequestConfig( TIM4_UPDATESOURCE_GLOBAL);
	TIM4_Cmd(ENABLE );
}
void SYSTEM_TIM2_PWM_Init(void)
{
	 /* Time base configuration */
	TIM2_TimeBaseInit(TIM2_PRESCALER_16,12);
	/* PWM1 Mode configuration: Channel2 */ 
	TIM2_OC3Init(TIM2_OCMODE_PWM1, TIM2_OUTPUTSTATE_ENABLE,5, TIM2_OCPOLARITY_HIGH);
	TIM2_OC3PreloadConfig(ENABLE);
	TIM2_ARRPreloadConfig(ENABLE);
	//TIM2_Cmd(ENABLE);
	TIM2_PWM_DISABLE();

}
void SYSTEM_Function_Init(void)
{
	CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV2);//MCU MAIN CLOCK=8MHZ 
	
	SYSTEM_Led_Init(); 

	SYSTEM_IR_Init();
		
	KEY_Init();

	SYSTEM_TIM1_Init();
	
	SYSTEM_TIM4_Init();

	SP_INIT(&(System_Para.sp_thd));
	TimeOutDet_Init();
	
	SYSTEM_TIM2_PWM_Init();
}

void SYSTEM_LED_Handle(void)
{
	switch(System_Para.System_Led_Status)
	{
		case SYSTEM_LED_ALL_OFF:
		{
			System_Para._1HourClose_Flag=False;
			System_Para._2HourClose_Flag=False;
			System_Para._3HourClose_Flag=False;
			System_Para._4HourClose_Flag=False;
			System_Para.CloseCnt_1S=0;
			
			GPIO_WriteLow(_SYSTEM_1H_LED_PORT,_SYSTEM_1H_LED_PIN);
			GPIO_WriteLow(_SYSTEM_2H_LED_PORT,_SYSTEM_2H_LED_PIN);
			GPIO_WriteLow(_SYSTEM_3H_LED_PORT,_SYSTEM_3H_LED_PIN);
			GPIO_WriteLow(_SYSTEM_4H_LED_PORT,_SYSTEM_4H_LED_PIN);
			break;
		}
		case SYSTEM_1H_LED_ON:
		{
			System_Para._1HourClose_Flag=True;
			System_Para._2HourClose_Flag=False;
			System_Para._3HourClose_Flag=False;
			System_Para._4HourClose_Flag=False;
			System_Para.CloseCnt_1S=0;
			
			GPIO_WriteHigh(_SYSTEM_1H_LED_PORT,_SYSTEM_1H_LED_PIN);
			GPIO_WriteLow(_SYSTEM_2H_LED_PORT,_SYSTEM_2H_LED_PIN);
			GPIO_WriteLow(_SYSTEM_3H_LED_PORT,_SYSTEM_3H_LED_PIN);
			GPIO_WriteLow(_SYSTEM_4H_LED_PORT,_SYSTEM_4H_LED_PIN);
			
			break;
		}
		case SYSTEM_2H_LED_ON:
		{	
			System_Para._1HourClose_Flag=False;
			System_Para._2HourClose_Flag=True;
			System_Para._3HourClose_Flag=False;
			System_Para._4HourClose_Flag=False;
			System_Para.CloseCnt_1S=0;
			
			GPIO_WriteLow(_SYSTEM_1H_LED_PORT,_SYSTEM_1H_LED_PIN);
			GPIO_WriteHigh(_SYSTEM_2H_LED_PORT,_SYSTEM_2H_LED_PIN);
			GPIO_WriteLow(_SYSTEM_3H_LED_PORT,_SYSTEM_3H_LED_PIN);
			GPIO_WriteLow(_SYSTEM_4H_LED_PORT,_SYSTEM_4H_LED_PIN);
			break;
		}
		case SYSTEM_3H_LED_ON:
		{	
			System_Para._1HourClose_Flag=False;
			System_Para._2HourClose_Flag=False;
			System_Para._3HourClose_Flag=True;
			System_Para._4HourClose_Flag=False;
			System_Para.CloseCnt_1S=0;
			
			GPIO_WriteLow(_SYSTEM_1H_LED_PORT,_SYSTEM_1H_LED_PIN);
			GPIO_WriteLow(_SYSTEM_2H_LED_PORT,_SYSTEM_2H_LED_PIN);
			GPIO_WriteHigh(_SYSTEM_3H_LED_PORT,_SYSTEM_3H_LED_PIN);
			GPIO_WriteLow(_SYSTEM_4H_LED_PORT,_SYSTEM_4H_LED_PIN);
			break;
		}
		case SYSTEM_4H_LED_ON:
		{
			System_Para._1HourClose_Flag=False;
			System_Para._2HourClose_Flag=False;
			System_Para._3HourClose_Flag=False;
			System_Para._4HourClose_Flag=True;
			System_Para.CloseCnt_1S=0;
			
			GPIO_WriteLow(_SYSTEM_1H_LED_PORT,_SYSTEM_1H_LED_PIN);
			GPIO_WriteLow(_SYSTEM_2H_LED_PORT,_SYSTEM_2H_LED_PIN);
			GPIO_WriteLow(_SYSTEM_3H_LED_PORT,_SYSTEM_3H_LED_PIN);
			GPIO_WriteHigh(_SYSTEM_4H_LED_PORT,_SYSTEM_4H_LED_PIN);			
			break;
		}
		default:
		{
			System_Para.System_Led_Status=SYSTEM_LED_ALL_OFF;
			break;
		}
	}	
}

int SYSTEM_IR_SendHandle(u16 Synccode,u16 Datacode,u16 Factorycode)
{
	TIM2_PWM_ENABLE();
	SP_BEGIN(&(System_Para.sp_thd));
	// START
	SYSTEM_IR_LOW();
	TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_START_LOW);
	SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
	SYSTEM_IR_HIGHT();
	TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_START_HIGHT);
	SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
	//SYNC
	for(System_Para.Synccode_Cnt=0;System_Para.Synccode_Cnt<_SYSTEM_IR_DATA_NUM;System_Para.Synccode_Cnt++)
	{
		Synccode=Synccode<<System_Para.Synccode_Cnt;
		if(Synccode&0x8000)
		{
			SYSTEM_IR_LOW();
			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
			SYSTEM_IR_HIGHT();
			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_1);
			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
		}
		else
		{
			SYSTEM_IR_LOW();
			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
			SYSTEM_IR_HIGHT();
			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_0);
			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
		}
		
	}
	// DATA
	for(System_Para.Datacode_Cnt=0;System_Para.Datacode_Cnt<_SYSTEM_IR_DATA_NUM;System_Para.Datacode_Cnt++)
	{
		Datacode=Datacode<<System_Para.Datacode_Cnt;
		if(Datacode&0x8000)
		{
			SYSTEM_IR_LOW();
			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
			SYSTEM_IR_HIGHT();
			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_1);
			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
		}
		else
		{
			SYSTEM_IR_LOW();
			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
			SYSTEM_IR_HIGHT();
			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_0);
			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
		}

	}
	// Factory code
	for(System_Para.Factorycode_Cnt=0;System_Para.Factorycode_Cnt<_SYSTEM_IR_DATA_NUM;System_Para.Factorycode_Cnt++)
	{
		Factorycode=Factorycode<<System_Para.Factorycode_Cnt;
		if(Factorycode&0x8000)
		{
			SYSTEM_IR_LOW();
			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
			SYSTEM_IR_HIGHT();
			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_1);
			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
		}
		else
		{
			SYSTEM_IR_LOW();
			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
			SYSTEM_IR_HIGHT();
			TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_0);
			SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
		}
	}
	//STOP
	SYSTEM_IR_LOW();
	TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
	SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
	SYSTEM_IR_HIGHT();
	TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_STOP_HIGHT);
	SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
	SYSTEM_IR_LOW();
	TimeOut_Record(&(System_Para.timer),_SYSTEM_IR_DATA_LOW);
	SP_WAIT_UNTIL(&(System_Para.sp_thd), TimeOutDet_Check(&(System_Para.timer)));
	SYSTEM_IR_HIGHT();
	
	System_Para.Succeed_SendFlag=True;
	System_Para.AutoCloseFlag=False;
	TIM2_PWM_DISABLE();
	SP_END(&(System_Para.sp_thd));
	
}


int SYSTEM_SEND_OPEN(void)
{
	SYSTEM_IR_SendHandle(_SYSTEM_IR_SYNCCODE,_SYSTEM_IR_OPENCODE,_SYSTEM_IR_STOPCODE);
}
int SYSTEM_SEND_CLOSE(void)
{
	SYSTEM_IR_SendHandle(_SYSTEM_IR_SYNCCODE,_SYSTEM_IR_CLOSECODE,_SYSTEM_IR_STOPCODE);
}
int SYSTEM_TimeToClose_Handle(void)
{
	if(System_Para.AutoCloseFlag)
	{
		SYSTEM_SEND_CLOSE();
		
		System_Para.OpenFlag=False;
		Key_Para.Key_ShortPressFlag=False;
		System_Para.Device_Status=_SYSTEM_DEVICE_OFF;
		System_Para.System_Led_Status=SYSTEM_LED_ALL_OFF;
		SYSTEM_LED_Handle();
	}

}
void SYSTEM_TimeToClose_IQRHandle(void)
{
	if(System_Para._4HourClose_Flag)
	{
		if(System_Para.CloseCnt_1S++>1000)
		{
			System_Para.CloseCnt_1S=0;
			if(System_Para.CloseCnt++>=_SYSTEM_CLOSE_1HOUR)
			{
				System_Para.CloseCnt=0;
				System_Para.CloseCnt_1S=0;
				System_Para.System_Led_Status=SYSTEM_3H_LED_ON;
				SYSTEM_LED_Handle();
			}
		}
	}
	if(System_Para._3HourClose_Flag)
	{
		if(System_Para.CloseCnt_1S++>1000)
		{
			System_Para.CloseCnt_1S=0;
			if(System_Para.CloseCnt++>=_SYSTEM_CLOSE_1HOUR)
			{
				System_Para.CloseCnt=0;
				System_Para.CloseCnt_1S=0;
				System_Para.System_Led_Status=SYSTEM_2H_LED_ON;
				SYSTEM_LED_Handle();
			}
		}
	}
	if(System_Para._2HourClose_Flag)
	{
		if(System_Para.CloseCnt_1S++>1000)
		{
			System_Para.CloseCnt_1S=0;
			if(System_Para.CloseCnt++>=_SYSTEM_CLOSE_1HOUR)
			{
				System_Para.CloseCnt=0;
				System_Para.CloseCnt_1S=0;
				System_Para.System_Led_Status=SYSTEM_1H_LED_ON;
				SYSTEM_LED_Handle();
			}
		}
	}
	if(System_Para._1HourClose_Flag)
	{
		if(System_Para.CloseCnt_1S++>1000)
		{
			System_Para.CloseCnt_1S=0;
			if(System_Para.CloseCnt++>=_SYSTEM_CLOSE_1HOUR)
			{
				System_Para.CloseCnt=0;
				System_Para.CloseCnt_1S=0;
				System_Para.System_Led_Status=SYSTEM_LED_ALL_OFF;
				SYSTEM_LED_Handle();
				System_Para.AutoCloseFlag=True;
				
			}
		}
	}

}
void SYSTEM_Handle(void)
{
	SYSTEM_TimeToClose_Handle();
	
	if(Key_Para.Key_ShortPressFlag)
	{
		System_Para.OpenFlag=~System_Para.OpenFlag;
		System_Para.Succeed_SendFlag=False;
		Key_Para.Key_ShortPressFlag=False;
	}
	if(System_Para.OpenFlag)
	{
		if(System_Para.Succeed_SendFlag==False)
		{
			SYSTEM_SEND_OPEN();
			System_Para.Device_Status=_SYSTEM_DEVICE_ON; // �յ�����״̬ 
		}
	}
	else
	{
		if(System_Para.Succeed_SendFlag==False)
		{
			SYSTEM_SEND_CLOSE();
			System_Para.Device_Status=_SYSTEM_DEVICE_OFF;
		}
	}

	if(System_Para.Device_Status==_SYSTEM_DEVICE_ON)
	{
		if(Key_Para.Key_DoublePressFlag)
		{
			Key_Para.Key_DoublePressFlag=False;
			if(++System_Para.System_Led_Status==SYSTEM_USELESS)
			{
				System_Para.System_Led_Status=SYSTEM_LED_ALL_OFF;
			}
			SYSTEM_LED_Handle();
		}
	}
	else
	{
		System_Para.System_Led_Status=SYSTEM_LED_ALL_OFF;
		SYSTEM_LED_Handle();
		if(Key_Para.Key_DoublePressFlag)
		{
			Key_Para.Key_DoublePressFlag=False;
		}
	}
}
